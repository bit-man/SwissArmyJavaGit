package edu.nyu.cs.javagit.client.cli;

import edu.nyu.cs.javagit.TestBase;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.Ref;
import edu.nyu.cs.javagit.api.commands.CommandResponse;
import edu.nyu.cs.javagit.api.commands.GitBranchOptions;
import edu.nyu.cs.javagit.test.utilities.TestProperty;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

public class CliGitBranchTest
        extends TestBase {
    private static final File DOES_NOT_CARE_PATH = new File(" /does/not/care");
    private static final Ref DOES_NOT_CARE_BRANCH = Ref.createBranchRef("DOES_NOT_CARE");
    private static final Ref DOES_NOT_CARE_BRANCH2 = Ref.createBranchRef("DOES_NOT_CARE2");

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private CliGitBranch client;
    private Validator validator;
    private IGitProcessBuilder processBuilder;
    private List<String> expectedCommand;

    @Before
    public void setup()
            throws IOException, JavaGitException {
        super.setUp();
        validator = mock(Validator.class);
        processBuilder = new ProcessBuilderMock();
    }

    @Test
    public void onNullRepositoryPathThrowsException()
            throws IOException, JavaGitException {
        givenNewClient();
        doThrow(RuntimeException.class).when(validator).checkNullArgument(any(), anyString());
        whenExpectedExceptionIs(RuntimeException.class);

        whenBranchIsCreatedWith();
    }

    @Test
    public void onBranchCreationCheckCommandLine()
            throws IOException, JavaGitException {
        givenNewClient();
        givenExpectedCommand(DOES_NOT_CARE_BRANCH);
        whenBranchIsCreatedWith();


        assertEquals(expectedCommand, processBuilder.getCommand());
    }

    @Test
    public void onBranchCreationWithOptionsCheckCommandLine()
            throws IOException, JavaGitException {
        givenNewClient();
        givenExpectedCommand(DOES_NOT_CARE_BRANCH, "-a");

        GitBranchOptions options = new GitBranchOptions();
        options.setOptA(true);
        whenBranchIsCreatedWith(options);

        assertEquals(expectedCommand, processBuilder.getCommand());
    }

    @Test
    public void onBranchCreationWithOptionsAndStartPointCheckCommandLine()
            throws IOException, JavaGitException {
        givenNewClient();
        givenExpectedCommand(DOES_NOT_CARE_BRANCH2, "-a");

        GitBranchOptions options = new GitBranchOptions();
        options.setOptA(true);
        whenBranchIsCreatedWithAndStratPoint(options, DOES_NOT_CARE_BRANCH2);

        assertEquals(expectedCommand, processBuilder.getCommand());
    }


    private void whenBranchIsCreatedWithAndStratPoint(GitBranchOptions options, Ref startingPoint) throws IOException, JavaGitException {
        client.createBranch(DOES_NOT_CARE_PATH, options, null, startingPoint);
    }

    private void givenExpectedCommand(Ref ref, String... args) {
        expectedCommand = new ArrayList<>();
        expectedCommand.add(TestProperty.GIT_PATH.asString() + "/git");
        expectedCommand.add("branch");
        Collections.addAll(expectedCommand, args);
        expectedCommand.add(ref.getName());
    }

    private void whenBranchIsCreatedWith(GitBranchOptions options)
            throws IOException, JavaGitException {
        client.createBranch(DOES_NOT_CARE_PATH, options, DOES_NOT_CARE_BRANCH);
    }

    private void whenBranchIsCreatedWith()
            throws IOException, JavaGitException {
        client.createBranch(DOES_NOT_CARE_PATH, DOES_NOT_CARE_BRANCH);
    }

    private void givenNewClient() {
        ICommandRunner<CommandResponse> commandRunner = new CommandRunnerMock<>();
        client = new CliGitBranch(configuration, validator, processBuilder, commandRunner);
    }


    private void whenExpectedExceptionIs(Class<? extends Throwable> exceptionClass) {
        exception.expect(exceptionClass);
    }

}
