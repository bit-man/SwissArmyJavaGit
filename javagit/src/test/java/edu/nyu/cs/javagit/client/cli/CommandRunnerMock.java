package edu.nyu.cs.javagit.client.cli;

import java.io.File;

public class CommandRunnerMock<T>
        implements ICommandRunner<T>
{
    @Override
    public T run() {
        return null;
    }

    @Override
    public void setWorkingDirectory(File repositoryPath)
    {
    }

    @Override
    public void setParser(IParser parser)
    {
    }

    @Override
    public void setProcessBuilder(IGitProcessBuilder processBuilder)
    {
    }
}
