/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.test.utilities;

import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.client.cli.ValidatorImpl;
import edu.nyu.cs.javagit.utilities.CheckUtilities;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test cases for testing the edu.nyu.cs.javagit.utilities.CheckUtilities class.
 */
public class TestCheckUtilities {

    private Validator validator;

    @Before
    public void setup() {
        validator = new ValidatorImpl();
    }

    @Test
    public void testCheckFileValidity() {
        final String unixFileDir = "/tmp/";
        final String windowsFileDir = "";
        final String filename1 = "bababa.3223.5asdbbw3gni.gagwe";
        final String filename2 = "baasdfbj65adaba.sdh3hs2s5y23.5asdbbw3gni.gagwe";

        String dirBase;
        if (System.getProperty("os.name").contains("indows")) {
            dirBase = windowsFileDir;
        } else {
            dirBase = unixFileDir;
        }

        // Find a test filename that doesnot exist.
        String fullFilePath = dirBase + filename1;
        File file = new File(fullFilePath);
        if (file.exists()) {
            fullFilePath = dirBase + filename2;
            file = new File(fullFilePath);
            if (file.exists()) {
                fail("In checking for file validity, the two test filenames are already in existance.");
            }
        }

        try {
            cleanup(file.createNewFile(), "Unable to create temporary file to test checkFileValidity");
        } catch (IOException e) {
            e.printStackTrace();
            fail("Unable to create temporary file to test checkFileValidity");
        }

        cleanup(file.delete(), "Unable to delete temporary file after testing checkFileValidity");
    }

    private void cleanup(boolean delete, String s) {
        if (!delete) {
            fail(s);
        }
    }

    @Test
    public void testCheckNullArgument() {
        // Test invalid values.
        try {
            CheckUtilities.checkNullArgument(null, "Test variable name");
            fail("No NPE thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("NPE didn't contain expected message.  Error!",
                    "000003: An Object argument was not specified but is required.  "
                            + "{ variableName=[Test variable name] }", e.getMessage());
        }

        // Test valid values.
        try {
            CheckUtilities.checkNullArgument("There is an object here.", "Test variable name");
        } catch (NullPointerException e) {
            fail("Caught NPE when none was expected.  Error!");
        }
    }

    @Test
    public void testCheckObjectEquals() {
        // Test for false results
        String s1 = "test me!";
        String s2 = null;
        assertFalse(CheckUtilities.checkObjectsEqual(s1, s2));

        s2 = "test";
        assertFalse(CheckUtilities.checkObjectsEqual(s1, s2));

        s1 = null;
        assertFalse(CheckUtilities.checkObjectsEqual(s1, s2));

        // Test for true results
        s1 = "test me!";
        s2 += " me!";
        assertTrue(CheckUtilities.checkObjectsEqual(s1, s2));
    }

    @Test
    public void testCheckStringArgument() {
        // Test invalid values
        assertCheckStringArgumentThrowsNPE(null, "aVariableName",
                "000001: A String argument was not specified but is required.  "
                        + "{ variableName=[aVariableName] }");

        assertCheckStringArgumentThrowsIllegalArgException("", "aVariableName",
                "000001: A String argument was not specified but is required.  "
                        + "{ variableName=[aVariableName] }");

        // Test valid values
        assertCheckStringArgumentIsValid("str", "SomeVarName");
    }

    private void assertCheckStringArgumentIsValid(String str, String variableName) {
        try {
            CheckUtilities.checkStringArgument(str, variableName);
        } catch (NullPointerException e) {
            fail("NPE exception thrown when one was not expected!");
        } catch (IllegalArgumentException e) {
            fail("IllegalArgumentException exception thrown when one was not expected!");
        }
    }

    private void assertCheckStringArgumentThrowsNPE(String str, String variableName, String message) {
        try {
            CheckUtilities.checkStringArgument(str, variableName);
            fail("NPE exception not thrown when one was expected!");
        } catch (NullPointerException e) {
            assertEquals("NPE did not contain the expected message.", message, e.getMessage());
        }
    }

    private void assertCheckStringArgumentThrowsIllegalArgException(String str, String variableName,
                                                                    String message) {
        try {
            CheckUtilities.checkStringArgument(str, variableName);
            fail("IllegalArgumentException not thrown when one was expected!");
        } catch (IllegalArgumentException e) {
            assertEquals("IllegalArgumentException did not contain the expected message.", message, e
                    .getMessage());
        }
    }

    @Test
    public void testCheckUnorderedListsEqual() {
        // Test for false results
        List<String> l1 = new ArrayList<>();
        List<String> l2 = null;
        assertFalse(validator.checkUnorderedListsEqual(l1, l2));

        l1 = null;
        l2 = new ArrayList<>();
        assertFalse(validator.checkUnorderedListsEqual(l1, l2));

        l1 = new ArrayList<>();
        l1.add("str");
        assertFalse(validator.checkUnorderedListsEqual(l1, l2));

        String s = "st";
        s += "r";
        l2.add(s);
        l2.add("Bing Bop");
        assertFalse(validator.checkUnorderedListsEqual(l1, l2));

        s = "Bing";
        l1.add(s);
        assertFalse(validator.checkUnorderedListsEqual(l1, l2));

        // Test for true results
        l1.remove(1);
        s += " Bop";
        l1.add(s);
        assertTrue(validator.checkUnorderedListsEqual(l1, l2));
    }

}
