/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api;

import edu.nyu.cs.javagit.TestBase;
import edu.nyu.cs.javagit.client.cli.Validator;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * Test cases for our <code>WorkingTree</code> class.
 */
public class TestWorkingTree extends TestBase {
    // We want two different strings that represent the same place in the filesystem.
    private static final String TEST_DIRNAME = "testdir";

    @Test
    public void testWorkingTreeGetFile()
            throws JavaGitException {
        Validator validator = mock(Validator.class);
        WorkingTree workingTree = new WorkingTree(new File(TEST_DIRNAME), configuration, validator);
        File file = new File("file1");

        GitFile gitFile = workingTree.getFile(file);
        assertEquals(gitFile.getWorkingTree(), workingTree);

        GitDirectory gitDirectory = workingTree.getDirectory(file);
        assertEquals(gitDirectory.getWorkingTree(), workingTree);
    }

}
