package edu.nyu.cs.javagit;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import edu.nyu.cs.javagit.api.Configuration;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.client.cli.ValidatorImpl;
import edu.nyu.cs.javagit.test.utilities.RealDeletor;
import edu.nyu.cs.javagit.test.utilities.TestProperty;

/**
 * Description : Base class for testing
 * Date: 3/30/13
 * Time: 2:14 AM
 */
@Ignore
public class TestBase {

    private RealDeletor d = null;
    protected Configuration configuration;
    protected Validator validator;

    @Before
    public void setUp() throws IOException, JavaGitException {
        String gitPath = TestProperty.GIT_PATH.asString();
        validator = new ValidatorImpl();
        configuration = new Configuration(gitPath, validator);
    }


    @After
    public void tearDown() {
        try {
            getDeletor().delete();
        } catch (JavaGitException e) {
            e.printStackTrace();
        }
    }

    protected RealDeletor getDeletor() {
        if ( d == null)
            d = new RealDeletor();

        return d;
    }
}
