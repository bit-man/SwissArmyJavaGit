/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api.commands;

import edu.nyu.cs.javagit.TestBase;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.test.utilities.FileUtilities;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import static org.mockito.Mockito.mock;

public class TestGitAdd extends TestBase {

    File repoDirectory;
    GitAdd gitAdd;
    GitStatus gitStatus;
    private Validator validator;

    @Before
    public void setUp() throws IOException, JavaGitException {
        super.setUp();
        validator = mock(Validator.class);
        repoDirectory = FileUtilities.createTempDirectory("GitCommitTestRepo");
        getDeletor().add(repoDirectory);
        GitInit gitInit = new GitInit(configuration, validator);
        gitInit.init(repoDirectory);
        gitAdd = new GitAdd(configuration, validator);
        gitStatus = new GitStatus(configuration, validator);
    }

    /**
     * Test for adding couple of files and a directory to the repository in verbose mode and then
     * verifying that these are in fact added by &lt;git-add&gt; command.
     *
     * @throws IOException      thrown -
     *                          <ul>
     *                          <li> if the files do not physically exist.
     *                          <li> if the files do not have proper permissions.
     *                          </ul>
     */
    @Test
    public void testAddingFilesToRepository()
            throws IOException {
        File file1 = FileUtilities.createFile(repoDirectory, "fileB.txt", "This is file fileB.txt");
        File tmpDir = new File(repoDirectory.getAbsolutePath() + File.separator + "dirA");
        createFileInDirectory(file1, tmpDir, this::testAddingFilesToRepositorySuccessful, this::failedMkDir);
    }

    private void createFileInDirectory(File file1, File tmpDir, Consumer<File> success, Consumer<File> fail) {
        if (tmpDir.mkdir()) {
            success.accept(file1);
        } else {
            fail.accept(tmpDir);
        }
    }

    private void failedMkDir(File tmpDir) {
        fail("Unable to create directory: " + tmpDir);
    }

    private void testAddingFilesToRepositorySuccessful(File file1) {
        try {
            File file2 = FileUtilities.createFile(repoDirectory, "dirA/fileB.txt", "Sample Contents");

            List<File> paths = new ArrayList<>();
            paths.add(file1);
            paths.add(new File("dirA"));
            paths.add(file2);
            GitAddOptions options = new GitAddOptions();
            options.setVerbose(true);
            GitAddResponse response = gitAdd.add(repoDirectory, options, paths);
            assertEquals("Number of files added is incorrect", 2, response.getFileListSize());
        } catch (IOException | JavaGitException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAddingOneFileToRepository()
            throws IOException {
        File file1 = FileUtilities.createFile(repoDirectory, "fileA.txt", "This is file fileA.txt");
        File tmpDir = new File(repoDirectory.getPath() + File.separator + "dirA");
        createFileInDirectory(file1, tmpDir, this::testAddingOneFileToRepositorySuccess2, this::failedMkDir);
    }

    @Test
    @Ignore
    public void testAddingOneFileToRepositoryBlanksInPath() throws IOException, JavaGitException {
        File file1 = FileUtilities.createFile(repoDirectory, "file A.txt", "This is file fileA.txt");
        gitAdd.add(repoDirectory, file1);
        GitStatusResponse statusResponse = gitStatus.status(repoDirectory);
        assertEquals("File to commit", 1, statusResponse.getNewFilesToCommitSize());
        assertEquals("Wrong path, ", repoDirectory.getAbsolutePath() + File.separator + "file A.txt", statusResponse.getNewFilesToCommit().iterator().next().getAbsolutePath());
    }


    @Test
    public void testAddingFilesToRepositoryWithNoOptions()
            throws IOException {
        File file1 = FileUtilities.createFile(repoDirectory, "fileA.txt", "This is file fileA.txt");
        File tmpDir = new File(repoDirectory.getAbsolutePath() + File.separator + "dirA");
        createFileInDirectory(file1, tmpDir, this::successTestAddingFilesToRepositoryWithNoOptions, this::failedMkDir);
    }

    private void successTestAddingFilesToRepositoryWithNoOptions(File file1) {
        try {
            File file2 = FileUtilities.createFile(repoDirectory, "dirA" + File.separator + "fileB.txt",
                    "Sample Contents fileB.txt");
            List<File> paths = new ArrayList<>();
            paths.add(file1);
            paths.add(new File("dirA"));
            paths.add(file2);
            gitAdd.add(repoDirectory, paths);
            GitStatusResponse statusResponse = gitStatus.status(repoDirectory);
            assertEquals("File to commit", 2, statusResponse.getNewFilesToCommitSize());
        } catch (IOException | JavaGitException e) {
            fail(e.getMessage());
        }
    }

    private void testAddingOneFileToRepositorySuccess2(File file1) {
        try {
            File file2 = FileUtilities.createFile(repoDirectory, "dirA" + File.separator + "fileB.txt",
                    "Sample Contents fileB.txt");
            gitAdd.add(repoDirectory, file1);
            gitAdd.add(repoDirectory, new File("dirA"));
            gitAdd.add(repoDirectory, file2);
            GitStatusResponse statusResponse = gitStatus.status(repoDirectory);
            assertEquals("File to commit", 2, statusResponse.getNewFilesToCommitSize());
        } catch (IOException | JavaGitException e) {
            fail(e.getMessage());
        }
    }

}