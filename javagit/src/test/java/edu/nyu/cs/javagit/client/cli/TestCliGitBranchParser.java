/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.client.cli;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import edu.nyu.cs.javagit.TestBase;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.Ref;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse.BranchRecord;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse.ResponseType;
import edu.nyu.cs.javagit.client.GitBranchResponseImpl;

public class TestCliGitBranchParser
        extends TestBase {

	private Collection<String> output = new ArrayList<>();

    @Test
    public void testBranchDeletion() {
        CliGitBranch.GitBranchParser parser = new CliGitBranch.GitBranchParser(validator);

        parser.parseLine("Deleted branch branchA.");
        parser.parseLine("Deleted branch branchB.");
        GitBranchResponseImpl expected = new GitBranchResponseImpl(validator);
        expected.setResponseType(ResponseType.MESSAGE);
        expected.addMessages("Deleted branch");
        expected.addIntoBranchList(Ref.createBranchRef("branchA"));
        expected.addIntoBranchList(Ref.createBranchRef("branchB"));
        assertResponsesEqual(parser, expected);
    }

    @Test
    public void testBranchList() {
        CliGitBranch.GitBranchParser parser = new CliGitBranch.GitBranchParser(validator);
        parser.parseLine("doc-refactor");
        parser.parseLine("master");
        parser.parseLine("refactor");
        parser.parseLine("* xyz");
        GitBranchResponseImpl expected = new GitBranchResponseImpl(validator);
        expected.addIntoBranchList(Ref.createBranchRef("doc-refactor"));
        expected.addIntoBranchList(Ref.createBranchRef("master"));
        expected.addIntoBranchList(Ref.createBranchRef("refactor"));
        expected.addIntoBranchList(Ref.createBranchRef("xyz"));
        expected.setCurrentBranch(Ref.createBranchRef("xyz"));
        expected.setResponseType(ResponseType.BRANCH_LIST);
        assertResponsesEqual(parser, expected);
    }

    @Test
    public void testBranchListVerbose() {
        CliGitBranch.GitBranchParser parser = new CliGitBranch.GitBranchParser(validator);
        parser.parseLine("doc-refactor 2a66ab6 Committing a document");
        parser.parseLine("master       23d48c1 new file");
        parser.parseLine("refactor     2a66ab6 Committing a document");
        parser.parseLine("* xyz          03529b8 commit");

        GitBranchResponseImpl expected = new GitBranchResponseImpl(validator);
        BranchRecord record = new BranchRecord(Ref.createBranchRef("doc-refactor"),
                Ref.createSha1Ref("2a66ab6"), "Committing a document", false);
        expected.addIntoBranchList(Ref.createBranchRef("doc-refactor"));
        expected.addIntoListOfBranchRecord(record);
        record = new BranchRecord(Ref.createBranchRef("master"), Ref.createSha1Ref("23d48c1"),
                "new file", false);
        expected.addIntoBranchList(Ref.createBranchRef("master"));
        expected.addIntoListOfBranchRecord(record);
        record = new BranchRecord(Ref.createBranchRef("refactor"), Ref.createSha1Ref("2a66ab6"),
                "Committing a document", false);
        expected.addIntoBranchList(Ref.createBranchRef("refactor"));
        expected.addIntoListOfBranchRecord(record);
        record = new BranchRecord(Ref.createBranchRef("xyz"), Ref.createSha1Ref("03529b8"),
                "commit", true);
        expected.addIntoBranchList(Ref.createBranchRef("xyz"));
        expected.addIntoListOfBranchRecord(record);
        expected.setCurrentBranch(Ref.createBranchRef("xyz"));
        expected.setResponseType(ResponseType.BRANCH_LIST);
        assertResponsesEqual(parser, expected);
    }

    @Test
    public void testGitBranchParserErrorInput() {
        CliGitBranch.GitBranchParser parser = new CliGitBranch.GitBranchParser(validator);
        parser.parseLine("error: branch 'to_be_deleted' not found.");

        assertExceptionThrownOnResponseRetrieval(parser,
                "404000: Error calling git-branch.   The git-branch error message:  { "
                        + "line1=[error: branch 'to_be_deleted' not found.] }");

        parser = new CliGitBranch.GitBranchParser(validator);
        parser.parseLine("error: Cannot delete the branch 'xyz' which you are currently on.");

        assertExceptionThrownOnResponseRetrieval(parser,
                "404000: Error calling git-branch.   The git-branch error message:  { "
                        + "line1=[error: Cannot delete the branch 'xyz' which you are currently on.] }");
    }

    private void assertResponsesEqual(CliGitBranch.GitBranchParser parser,
                                      GitBranchResponseImpl expected) {
        try {
			assertEquals("Expected GitBranchResponse not equal to actual GitBranchResponse.", expected, parser.getResponse(output));
        } catch (JavaGitException e) {
            fail("Getting a GitBranchResponse from a CliGitBranch.GitBranchParser instance threw an exception when it should not have.");
        }
    }

    private void assertExceptionThrownOnResponseRetrieval(CliGitBranch.GitBranchParser parser,
                                                          String message) {
        try {
			parser.getResponse(output);
			fail("Got GitBranchResponse from GitBranchParser when an exception should have been thrown!");
        } catch (JavaGitException e) {
            assertEquals("Exception message is not correct for thrown exception.", e.getMessage(), message);
            assertEquals("JavaGitException code is not correct", e.getCode(), 404000);
        }
    }
}
