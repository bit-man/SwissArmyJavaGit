package edu.nyu.cs.javagit.client.cli;

import edu.nyu.cs.javagit.api.Configuration;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.commands.CommandResponse;
import edu.nyu.cs.javagit.test.utilities.TestProperty;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

public class CliGitStatusTest {
    private static final File REPOSITORY_PATH_DOESNT_CARE = new File("");
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private CliGitStatus client;
    private IGitProcessBuilder processBuilder;
    private Validator validator;

    @Before
    public void setup() {
        validator = mock(Validator.class);
    }

    @Test
    public void invalidRepositoryPath()
            throws IOException, JavaGitException {
        givenValidationFailsWithException(RuntimeException.class);
        givenNewClient();
        whenExpectedExceptionIs(RuntimeException.class);
        thenStatusShouldFail(REPOSITORY_PATH_DOESNT_CARE);
    }

    @Test
    public void commandLineContainsStatusCommand()
            throws IOException, JavaGitException
    {
        givenNewClient();
        whenStatusIsQueried();
        thenCommandlineContains("status");
    }

    @Test
    public void commandLineContainsPorcelainSwitch()
            throws IOException, JavaGitException
    {
        givenNewClient();
        whenStatusIsQueried();
        thenCommandlineContains("--porcelain");
    }

    @Test
    public void commandLineContainsIgnoredSwitch()
            throws IOException, JavaGitException
    {
        givenNewClient();
        whenStatusIsQueried();
        thenCommandlineContains("--ignored");
    }

    private void thenCommandlineContains(String command)
    {
        assertThat(processBuilder.getCommand()).containsOnlyOnce(command);
    }

    private void whenStatusIsQueried()
            throws IOException, JavaGitException
    {
        client.status(REPOSITORY_PATH_DOESNT_CARE);
    }


    private void whenExpectedExceptionIs(Class<? extends Throwable> exceptionClass)
    {
        exception.expect(exceptionClass);
    }

    private void thenStatusShouldFail(File repositoryPath)
            throws JavaGitException, IOException
    {
        client.status(repositoryPath);
    }

    private void givenNewClient()
            throws JavaGitException, IOException {
        this.processBuilder = new ProcessBuilderMock();
        ICommandRunner<CommandResponse> commandRunner = new CommandRunnerMock<>();
        Configuration configuration = new Configuration(TestProperty.GIT_PATH.asString(), validator);
        client = new CliGitStatus(configuration, validator, processBuilder, commandRunner);
    }

    private void givenValidationFailsWithException(Class<RuntimeException> toBeThrown)
            throws IOException {
        doThrow(toBeThrown).when(validator).checkFileValidity(any(File.class));
        doThrow(toBeThrown).when(validator).checkNullArgument(any(File.class), anyString());
    }


}
