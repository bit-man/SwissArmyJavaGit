/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api.commands;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.nyu.cs.javagit.TestBase;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.Ref;
import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.client.cli.ValidatorImpl;
import edu.nyu.cs.javagit.test.utilities.FileUtilities;

/**
 * Implements test cases for for GitBranch.
 */
public class TestGitBranch extends TestBase {
    private File repoDirectory;
    private GitBranch branch;
    private Ref branchA = Ref.createBranchRef("branchA");
    private Ref branchB = Ref.createBranchRef("branchB");

    private GitBranchOptions options;

    @Before
    public void setUp() throws IOException, JavaGitException {
        super.setUp();
        Validator validator = new ValidatorImpl();
        repoDirectory = FileUtilities.createTempDirectory("GitBranchTestRepo");
        getDeletor().add(repoDirectory);
        GitInit gitInit = new GitInit(configuration, validator);
        gitInit.init(repoDirectory);
        GitAdd add = new GitAdd(configuration, validator);
        branch = new GitBranch(configuration);
        options = new GitBranchOptions();

        addFileToRepo(add);
        commit(validator);
    }

    @Test
    public void testCreateBranch() throws IOException, JavaGitException {
        VoidResponse response = this.branch.createBranch(repoDirectory, branchA);
        assertNotNull(response);
    }

    @Test
    public void testDisplayBranch() throws IOException, JavaGitException {
        this.branch.createBranch(repoDirectory, branchA);
        this.branch.createBranch(repoDirectory, branchB);

        this.branch.branch(repoDirectory);

        options.setOptVerbose(true);
        this.branch.branch(repoDirectory, options);

        options.setOptVerbose(true);
        options.setOptNoAbbrev(true);
        this.branch.branch(repoDirectory, options);
    }

    @Test
    public void testRenameBranch() throws IOException, JavaGitException {
        Ref branchC = Ref.createBranchRef("branchC");
        branch.createBranch(repoDirectory, branchA);
        branch.createBranch(repoDirectory, branchB);

        branch.renameBranch(repoDirectory, false, branchA, branchC);

        branch.renameBranch(repoDirectory, true, branchB);
    }

    @Test
    public void testDeleteBranch() throws IOException, JavaGitException {
        branch.createBranch(repoDirectory, branchA);
        branch.createBranch(repoDirectory, branchB);

        branch.deleteBranch(repoDirectory, false, false, branchA);

        Ref branchC = Ref.createBranchRef("branchC");
        Ref branchD = Ref.createBranchRef("branchD");
        Ref branchE = Ref.createBranchRef("branchE");
        branch.createBranch(repoDirectory, branchC);
        branch.createBranch(repoDirectory, branchD);
        branch.createBranch(repoDirectory, branchE);

        List<Ref> branchList = new ArrayList<>();
        branchList.add(branchC);
        branchList.add(branchD);

        branch.deleteBranch(repoDirectory, false, false, branchList);
    }

    @Test
    public void testCreateBranchBadArgumentPassing() {
        assertCreateBranchNPEThrown(null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[repository path] }");
        assertCreateBranchNPEThrown(new File("SomePath"),
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[branch name] }");
        assertCreateBranchIllegalArgumentExceptionThrown(new File("SomePath"), new Ref(), new Ref(),
                "100000: Incorrect refType type.  { variableName=[branch name] }");
        assertCreateBranchIllegalArgumentExceptionThrown(new File("SomePath"), Ref
                        .createBranchRef("ref"), new Ref(),
                "100000: Incorrect refType type.  { variableName=[start point] }");
        assertCreateBranchWithOptsNPEThrown(null, null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[repository path] }");
        assertCreateBranchWithOptsNPEThrown(new File("SomePath"), null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[options] }");
        assertCreateBranchWithOptsNPEThrown(new File("SomePath"), new GitBranchOptions(),
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[branch name] }");
        assertCreateBranchOptsWithIllegalArgumentExceptionThrown(new File("SomePath"),
                new GitBranchOptions(), new Ref()
        );
        assertCreateBranchIllegalArgumentExceptionThrown(new File("SomePath"), Ref
                        .createBranchRef("ref"), new Ref(),
                "100000: Incorrect refType type.  { variableName=[start point] }");
    }

    @Test
    public void testBranchBadArgumentPassing() {
        assertBranchNPEThrown();
        assertBranchWithOptsNPEThrown(null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[repository path] }");
        assertBranchWithOptsNPEThrown(new File("SomePath"),
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[options] }");
    }

    @Test
    public void testRenameBranchBadArugmentPassing() {
        assertRenameBranchNPEThrown(null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[repository path] }");
        assertRenameBranchNPEThrown(new File("FileOne"),
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[new name] }");
        assertRenameBranchWithOldBranchNPEThrown(null, null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[repository path] }");
        assertRenameBranchWithOldBranchNPEThrown(new File("SomePath"), null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[old name] }");
        assertRenameBranchWithOldBranchNPEThrown(new File("SomePath"), new Ref(),
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[new name] }");
        assertRenameBranchIllegalArgumentExceptionThrown(new File("SomePath"), new Ref()
        );
        assertRenameBranchWithOldBranchIllegalArgumentExceptionThrown(new File("SomePath"), new Ref(),
                new Ref(), "100000: Incorrect refType type.  { variableName=[old name] }");
        assertRenameBranchWithOldBranchIllegalArgumentExceptionThrown(new File("SomePath"), Ref
                        .createBranchRef("ref"), new Ref(),
                "100000: Incorrect refType type.  { variableName=[new name] }");
    }

    @Test
    public void testDeleteBranchBadArugmentPassing() {
        assertDeleteBranchNPEThrown(null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[repository path] }");
        assertDeleteBranchNPEThrown(new File("FileOne"),
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[branch name] }");
        assertDeleteBranchIllegalArgumentExceptionThrown(new File("SomePath"), new Ref()
        );
        assertDeleteBranchListNPEThrown(null,
                "000003: An Object argument was not specified but is required.  "
                        + "{ variableName=[repository path] }");
        assertDeleteBranchListNPEThrown(new File("SomePath"),
                "000005: An List<?> argument was not specified or is empty but is required.  "
                        + "{ variableName=[branch list] }");
        List<Ref> branchList = new ArrayList<>();
        branchList.add(new Ref());
        assertDeleteBranchListIllegalArgumentExceptionThrown(new File("SomePath"), branchList
        );
    }

    private void commit(Validator validator)
            throws IOException, JavaGitException {
        GitCommit commit = new GitCommit(configuration, validator);
        commit.commit(repoDirectory, "Making a first test commit");
    }

    private void addFileToRepo(GitAdd add)
            throws IOException, JavaGitException {
        File testFile = FileUtilities.createFile(repoDirectory, "fileA.txt", "Sample Contents");
        List<File> filesToAdd = new ArrayList<>();
        filesToAdd.add(testFile);
        add.add(repoDirectory, null, filesToAdd);
    }

    private void assertCreateBranchNPEThrown(File repoPath, String expectedMessage) {
        try {
            branch.createBranch(repoPath, null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", expectedMessage, e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertCreateBranchIllegalArgumentExceptionThrown(File repoPath, Ref branchName,
                                                                  Ref startPoint, String expectedMessage) {
        try {
            branch.createBranch(repoPath, branchName, startPoint);
            fail("No IllegalArgumentException thrown when one was expected.  Error!");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "The message from the caught IllegalArgumentException is not what was expected!",
                    expectedMessage, e.getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertCreateBranchWithOptsNPEThrown(File repoPath, GitBranchOptions options,
                                                     String expectedMessage) {
        try {
            branch.createBranch(repoPath, options, null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", expectedMessage, e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertCreateBranchOptsWithIllegalArgumentExceptionThrown(File repoPath,
                                                                          GitBranchOptions options, Ref branchName) {
        try {
            branch.createBranch(repoPath, options, branchName);
            fail("No IllegalArgumentException thrown when one was expected.  Error!");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "The message from the caught IllegalArgumentException is not what was expected!",
                    "100000: Incorrect refType type.  { variableName=[branch name] }", e.getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertBranchNPEThrown() {
        try {
            branch.branch(null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", "000003: An Object argument was not specified but is required.  { variableName=[repository path] }", e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertBranchWithOptsNPEThrown(File repoPath,
                                               String expectedMessage) {
        try {
            branch.branch(repoPath, null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", expectedMessage, e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertRenameBranchNPEThrown(File repoPath, String expectedMessage) {
        try {
            branch.renameBranch(repoPath, false, null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", expectedMessage, e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertRenameBranchWithOldBranchNPEThrown(File repoPath, Ref oldBranch,
                                                          String expectedMessage) {
        try {
            branch.renameBranch(repoPath, false, oldBranch, null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", expectedMessage, e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertRenameBranchIllegalArgumentExceptionThrown(File repoPath, Ref newBranch) {
        try {
            branch.renameBranch(repoPath, false, newBranch);
            fail("No IllegalArgumentException thrown when one was expected.  Error!");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "The message from the caught IllegalArgumentException is not what was expected!",
                    "100000: Incorrect refType type.  { variableName=[new name] }", e.getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertRenameBranchWithOldBranchIllegalArgumentExceptionThrown(File repoPath,
                                                                               Ref oldBranch, Ref newBranch, String expectedMessage) {
        try {
            branch.renameBranch(repoPath, false, oldBranch, newBranch);
            fail("No IllegalArgumentException thrown when one was expected.  Error!");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "The message from the caught IllegalArgumentException is not what was expected!",
                    expectedMessage, e.getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertDeleteBranchNPEThrown(File repoPath, String expectedMessage) {
        try {
            branch.deleteBranch(repoPath, false, false, (Ref) null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", expectedMessage, e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertDeleteBranchListNPEThrown(File repoPath,
                                                 String expectedMessage) {
        try {
            branch.deleteBranch(repoPath, false, false, (List<Ref>) null);
            fail("No NullPointerException thrown when one was expected.  Error!");
        } catch (NullPointerException e) {
            assertEquals("The message from the caught NPE is not what was expected!", expectedMessage, e
                    .getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertDeleteBranchIllegalArgumentExceptionThrown(File repoPath,
                                                                  Ref branchName) {
        try {
            branch.deleteBranch(repoPath, false, false, branchName);
            fail("No IllegalArgumentException thrown when one was expected.  Error!");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "The message from the caught IllegalArgumentException is not what was expected!",
                    "100000: Incorrect refType type.  { variableName=[branch name] }", e.getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }

    private void assertDeleteBranchListIllegalArgumentExceptionThrown(File repoPath,
                                                                      List<Ref> branchList) {
        try {
            branch.deleteBranch(repoPath, false, false, branchList);
            fail("No IllegalArgumentException thrown when one was expected.  Error!");
        } catch (IllegalArgumentException e) {
            assertEquals(
                    "The message from the caught IllegalArgumentException is not what was expected!",
                    "100000: Incorrect refType type.  { variableName=[branch list] }", e.getMessage());
        } catch (Throwable e) {
            fail("Caught Throwable when none was expected.  Error!");
        }
    }
}
