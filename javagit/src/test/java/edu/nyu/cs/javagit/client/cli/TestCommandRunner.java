package edu.nyu.cs.javagit.client.cli;

import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.commands.CommandResponse;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class TestCommandRunner {

	private InputStream inputStream;
	private Integer exitCode;
	private Process process;
	private IGitProcessBuilder pb;
	private IParser parser;
	private CommandRunner<Object> runner;
	private Throwable response;

	@Test
	public void noWorkDirAvoidsProcessDirectorySet()
			throws IOException {
		givenProcessBuilder();
		whenCommandRunnerIsCreatedWithNoWorkdir();
		Mockito.verify(pb, times(0)).directory(any(File.class));
	}

	@Test
	public void runningProcessStartsIt()
			throws IOException, JavaGitException {
		givenProcessBuilder();
		givenEmptyInputStream();
		givenProcess();
		when(pb.start()).thenReturn(process);
		givenParser();

		whenCommandRunnerIsExecuted();

		Mockito.verify(pb, times(1)).start();
	}

	@Test
	public void runningProcessReturningNoInputAvoidsCallingParser() throws IOException, JavaGitException {
		givenProcessBuilder();
		givenEmptyInputStream();
		givenProcess();
		when(pb.start()).thenReturn(process);
		givenParser();

		whenCommandRunnerIsExecuted();

		Mockito.verify(parser, times(0)).parseLine(anyString());
	}

	@Test
	public void runningProcessMakesParserProcessExitValue() throws IOException, JavaGitException {
		givenEmptyInputStream();
		givenProcess();
		when(process.exitValue()).thenReturn(25);
		givenProcessBuilder();
		givenFullParser();

		whenCommandRunnerIsExecuted();

		assertThat(exitCode).isEqualTo(25);
	}

	@Test
	public void onBranchCreationFailureShouldGetOutput()
			throws IOException {
		givenEmptyFailingInputStream();
		givenProcess();
		givenProcessBuilder();
		givenFullParser();

		whenCommandRunnerIsCreated();
		response = catchThrowable(() -> runner.run());

		assertThat(response).isExactlyInstanceOf(JavaGitException.class);
		assertThat(((JavaGitException) response).getOutput()).hasSize(0);
	}

	private void whenCommandRunnerIsExecuted()
			throws IOException, JavaGitException {
		whenCommandRunnerIsCreated();
		runner.run();
	}

	private void whenCommandRunnerIsCreatedWithNoWorkdir() {
		new CommandRunner<>(parser, pb);
	}

	private void whenCommandRunnerIsCreated() {
		this.runner = new CommandRunner<>(new File(""), parser, pb);
	}

	private void givenParser() {
		this.parser = mock(IParser.class);
	}

	private void givenFullParser() {
		parser = new IParser() {

			@Override
			public void parseLine(String line) {

			}

			@Override
			public void processExitCode(int code) {
				exitCode = code;
			}

			@Override
			public CommandResponse getResponse(Collection<String> output) {
				return null;
			}

			@Override
			public void setWorkingDir(File workingDir) {
				throw new UnsupportedOperationException();
			}
		};
	}

	private void givenProcess() {
		this.process = mock(Process.class);
		when(process.getInputStream()).thenReturn(inputStream);
	}

	private void givenProcessBuilder()
			throws IOException {
		pb = mock(IGitProcessBuilder.class);
		when(pb.start()).thenReturn(process);
	}

	private void givenEmptyInputStream() {
		this.inputStream = new ProgrammableInputStream(Collections.singletonList(-1));
	}

	private void givenEmptyFailingInputStream() {
		this.inputStream = new ProgrammableInputStream(Collections.singletonList(ProgrammableInputStream.THROW_EXCEPTION_MARKER));
	}

	private static class ProgrammableInputStream
			extends InputStream {
		static final int THROW_EXCEPTION_MARKER = 1000;
		private int entry = 0;
		private final List<Integer> output;

		public ProgrammableInputStream(List<Integer> output) {
			this.output = output;
		}

		@Override
		public int read()
				throws IOException {
			Integer out = output.get(entry++);
			if (out == THROW_EXCEPTION_MARKER) {
				throw new IOException();
			}
			return out;
		}
	}
}
