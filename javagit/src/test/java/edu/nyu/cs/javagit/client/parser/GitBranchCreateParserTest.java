package edu.nyu.cs.javagit.client.parser;

import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.commands.CommandResponse;
import edu.nyu.cs.javagit.api.commands.VoidResponse;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

public class GitBranchCreateParserTest {

    private static final int SUCCESSFUL = 1;
    private static final int UNSUCCESSFUL = 0;
    private GitBranchCreateParser parser;
    private Collection<String> output;

    @Before
    public void setup() {
        parser = new GitBranchCreateParser();
    }

    @Test
    public void onErrorExitCodeThrowsExceptionWithCommandOutput() {

        givenExitCode(SUCCESSFUL);
        givenOutput();

        Throwable throwable = catchThrowable(() -> parser.getResponse(output));

        JavaGitException expectedException = new JavaGitException(404000, output);
        assertThat(throwable).usingComparator(forOutput()).isEqualTo(expectedException);
    }

    @Test
    public void onSuccessExitCodeReturnVoidResponse()
            throws JavaGitException {
        givenExitCode(UNSUCCESSFUL);
        givenOutput();

        CommandResponse response = parser.getResponse(output);

        assertThat(response).isExactlyInstanceOf(VoidResponse.class);
    }

    private Comparator<Throwable> forOutput() {
        return (o1, o2) -> {
            JavaGitException e2 = (JavaGitException) o2;
            JavaGitException e1 = (JavaGitException) o1;
            boolean equals = e1.getOutput().equals(e2.getOutput());
            return equals ? 0 : 1;
        };
    }

    private void givenOutput() {
        output = Arrays.asList("line 1", "line 2");
    }

    private void givenExitCode(int exitCode) {
        parser.processExitCode(exitCode);
    }

}
