package edu.nyu.cs.javagit.test.utilities;

import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.url.FileUrl;
import edu.nyu.cs.javagit.api.url.FtpUrl;
import edu.nyu.cs.javagit.api.url.HttpUrl;
import edu.nyu.cs.javagit.api.url.HttpsUrl;
import edu.nyu.cs.javagit.utilities.UrlMapper;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static junit.framework.Assert.assertEquals;

/**
 * Description : URL utilities testing
 * Date: 03-Mar-2014
 * Time: 7:43 PM
 */
public class TestUrlMapper {

    private UrlMapper urlmapper;

    @Before
    public void setup() {
        urlmapper = new UrlMapper();
    }

    @Test
    public void testFTP() throws MalformedURLException, JavaGitException {
        FtpUrl url = (FtpUrl) urlmapper.map(new URL("ftp", "host", "file"));
        assertEquals("ftp://host/file", url.toString());
    }

    @Test(expected = MalformedURLException.class)
    public void testFTPs()
            throws MalformedURLException {
        // FTPs is an unknown protocol to Java unless a custom handler is added
        new URL("ftps", "host", "file");
    }

    @Test
    public void testFile() throws MalformedURLException, JavaGitException {
        FileUrl url = (FileUrl) urlmapper.map(new URL("file", null, "myFile"));
        assertEquals("file:///myFile", url.toString());
    }

    @Test
    public void testHttps() throws MalformedURLException, JavaGitException {
        HttpsUrl url = (HttpsUrl) urlmapper.map(new URL("https", "host", "myFile"));
        assertEquals("https://host/myFile", url.toString());
    }

    @Test
    public void testHttp() throws MalformedURLException, JavaGitException {
        HttpUrl url = (HttpUrl) urlmapper.map(new URL("http", "host", "myFile"));
        assertEquals("http://host/myFile", url.toString());
    }

    @Test(expected = MalformedURLException.class)
    public void testSSH()
            throws MalformedURLException {

        // SSH is an unknown protocol to Java unless a custom handler is added
        new URL("ssh", "host", "myFile");
    }

    @Test(expected = MalformedURLException.class)
    public void testRsync()
            throws MalformedURLException {

        // RSYNC is an unknown protocol to Java unless a custom handler is added
        new URL("rsync", "host", "myFile");
    }


    @Test(expected = MalformedURLException.class)
    public void testGit()
            throws MalformedURLException {
        // GIT is an unknown protocol to Java unless a custom handler is added
        new URL("git", "host", "myFile");
    }
}
