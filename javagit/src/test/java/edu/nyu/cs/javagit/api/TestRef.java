/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api;

import edu.nyu.cs.javagit.TestBase;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertSame;
import static junit.framework.Assert.fail;

/**
 * Test case for the <code>Ref</code> data object.
 */
public class TestRef extends TestBase {

    // TODO (jhl388): add exhaustive tests of the Ref.equals() method
    // TODO (jhl388): add exhaustive tests of the Ref.hashcode() method

    @Test
    public void testStaticVariables() {
        // Checking HEAD
        assertEquals("Expected RefType of HEAD.", Ref.HEAD.getRefType(), Ref.RefType.HEAD);
        assertEquals(Ref.HEAD.getHeadOffset(), 0);

        // Checking HEAD_1
        assertEquals("Expected RefType of HEAD.", Ref.HEAD_1.getRefType(), Ref.RefType.HEAD);
        assertEquals(Ref.HEAD_1.getHeadOffset(), 1);
        assertNull(Ref.HEAD_1.getName());
    }

    @Test
    public void testCreateHeadRef() {
        // Testing invalid input
        assertIllegalCreateHeadRefArgument(-1,
                "000004: The int argument is not greater than the lower bound (lowerBound < toCheck).  "
                        + "{ toCheck=[-1], lowerBound=[-1], variableName=[headOffset] }");
        assertIllegalCreateHeadRefArgument(-23,
                "000004: The int argument is not greater than the lower bound (lowerBound < toCheck).  "
                        + "{ toCheck=[-23], lowerBound=[-1], variableName=[headOffset] }");

        // Testing valid input
        Ref ref = Ref.createHeadRef(0);
        assertEquals(ref, Ref.HEAD);
        assertSame(Ref.HEAD, ref);
        assertEquals(ref.toString(), "HEAD");

        ref = Ref.createHeadRef(1);
        assertEquals(ref, Ref.HEAD_1);
        assertSame(Ref.HEAD_1, ref);
        assertEquals(ref.toString(), "HEAD^1");

        ref = Ref.createHeadRef(2);
        assertEquals("Expected RefType of HEAD.", ref.getRefType(), Ref.RefType.HEAD);
        assertEquals(ref.getHeadOffset(), 2);
        assertNull(ref.getName());
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "HEAD~2");

        ref = Ref.createHeadRef(50);
        assertEquals("Expected RefType of HEAD.", ref.getRefType(), Ref.RefType.HEAD);
        assertEquals(ref.getHeadOffset(), 50);
        assertNull(ref.getName());
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "HEAD~50");
    }

    private void assertIllegalCreateHeadRefArgument(int headOffset, String expectedMessage) {
        try {
            Ref.createHeadRef(headOffset);
            fail("No IllegalArgumentException thrown when one was expected.  Error!");
        } catch (IllegalArgumentException e) {
            assertEquals("IllegalArgumentException didn't contain expected message.  Error!",
                    expectedMessage, e.getMessage());
        }
    }

    @Test
    public void testCreateBranchRef() {
        // Testing invalid input
        assertCreateBranchRefThrowsNPE(null,
                "000001: A String argument was not specified but is required.  { variableName=[name] }");
        assertCreateBranchRefThrowsIllegalArgException("",
                "000001: A String argument was not specified but is required.  { variableName=[name] }");

        // Testing valid input
        Ref ref = Ref.createBranchRef("master");
        Ref ref2 = Ref.createBranchRef("master");
        assertEquals("Expected RefType of BRANCH.", ref.getRefType(), Ref.RefType.BRANCH);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "master");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "master");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());

        ref = Ref.createBranchRef("myTestBranch");
        assertFalse(ref.equals(ref2));

        ref2 = Ref.createBranchRef("myTestBranch");
        assertEquals("Expected RefType of BRANCH.", ref.getRefType(), Ref.RefType.BRANCH);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "myTestBranch");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "myTestBranch");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());
    }

    private void assertCreateBranchRefThrowsNPE(String name, String message) {
        try {
            Ref.createBranchRef(name);
            fail("NPE exception not thrown when one was expected!");
        } catch (NullPointerException e) {
            assertEquals("NPE did not contain the expected message.", message, e.getMessage());
        }
    }

    private void assertCreateBranchRefThrowsIllegalArgException(String name, String message) {
        try {
            Ref.createBranchRef(name);
            fail("IllegalArgumentException not thrown when one was expected!");
        } catch (IllegalArgumentException e) {
            assertEquals("IllegalArgumentException did not contain the expected message.", message, e
                    .getMessage());
        }
    }

    @Test
    public void testCreateRemoteRef() {
        // Testing invalid input
        assertCreateRemoteRefThrowsNPE(null, null,
                "000001: A String argument was not specified but is required.  { variableName=[name] }");
        assertCreateRemoteRefThrowsIllegalArgException(null, "",
                "000001: A String argument was not specified but is required.  { variableName=[name] }");

        // Testing valid input
        Ref ref = Ref.createRemoteRef(null, "newFeatureDev");
        Ref ref2 = Ref.createRemoteRef(null, "newFeatureDev");
        assertEquals("Expected RefType of REMOTE.", ref.getRefType(), Ref.RefType.REMOTE);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "newFeatureDev");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "newFeatureDev");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());

        ref = Ref.createRemoteRef(null, "review0");
        assertFalse(ref.equals(ref2));

        ref2 = Ref.createRemoteRef("", "review0");
        assertEquals("Expected RefType of REMOTE.", ref.getRefType(), Ref.RefType.REMOTE);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "review0");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "review0");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());

        ref = Ref.createRemoteRef("", "nextReview");
        assertFalse(ref.equals(ref2));

        ref2 = Ref.createRemoteRef("", "nextReview");
        assertEquals("Expected RefType of REMOTE.", ref.getRefType(), Ref.RefType.REMOTE);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "nextReview");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "nextReview");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());

        ref = Ref.createRemoteRef("origin", "stable");
        assertFalse(ref.equals(ref2));

        ref2 = Ref.createRemoteRef("origin", "stable");
        assertEquals("Expected RefType of REMOTE.", ref.getRefType(), Ref.RefType.REMOTE);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "stable");
        assertEquals(ref.getRepositoryName(), "origin");
        assertEquals(ref.toString(), "origin/stable");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());

        ref = Ref.createRemoteRef("jhlRepo", "stable");
        assertFalse(ref.equals(ref2));

        ref2 = Ref.createRemoteRef("jhlRepo", "stable");
        assertEquals("Expected RefType of REMOTE.", ref.getRefType(), Ref.RefType.REMOTE);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "stable");
        assertEquals(ref.getRepositoryName(), "jhlRepo");
        assertEquals(ref.toString(), "jhlRepo/stable");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());
    }

    private void assertCreateRemoteRefThrowsNPE(String repositoryName, String name, String message) {
        try {
            Ref.createRemoteRef(repositoryName, name);
            fail("NPE exception not thrown when one was expected!");
        } catch (NullPointerException e) {
            assertEquals("NPE did not contain the expected message.", message, e.getMessage());
        }
    }

    private void assertCreateRemoteRefThrowsIllegalArgException(String repositoryName, String name,
                                                                String message) {
        try {
            Ref.createRemoteRef(repositoryName, name);
            fail("IllegalArgumentException not thrown when one was expected!");
        } catch (IllegalArgumentException e) {
            assertEquals("IllegalArgumentException did not contain the expected message.", message, e
                    .getMessage());
        }
    }

    @Test
    public void testCreateSha1Ref() {
        // Testing invalid input
        assertCreateSha1RefThrowsNPE(null,
                "000001: A String argument was not specified but is required.  { variableName=[name] }");
        assertCreateSha1RefThrowsIllegalArgException("",
                "000001: A String argument was not specified but is required.  { variableName=[name] }");

        // Testing valid input
        Ref ref = Ref.createSha1Ref("a");
        Ref ref2 = Ref.createSha1Ref("a");
        assertEquals("Expected RefType of SHA1.", ref.getRefType(), Ref.RefType.SHA1);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "a");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "a");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());

        ref = Ref.createSha1Ref("ab238dd4c9fa4d8eabe03715c3e8b212f9532013");
        assertFalse(ref.equals(ref2));

        ref2 = Ref.createSha1Ref("ab238dd4c9fa4d8eabe03715c3e8b212f9532013");
        assertEquals("Expected RefType of SHA1.", ref.getRefType(), Ref.RefType.SHA1);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "ab238dd4c9fa4d8eabe03715c3e8b212f9532013");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "ab238dd4c9fa4d8eabe03715c3e8b212f9532013");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());
    }

    private void assertCreateSha1RefThrowsNPE(String sha1Name, String message) {
        try {
            Ref.createSha1Ref(sha1Name);
            fail("NPE exception not thrown when one was expected!");
        } catch (NullPointerException e) {
            assertEquals("NPE did not contain the expected message.", message, e.getMessage());
        }
    }

    private void assertCreateSha1RefThrowsIllegalArgException(String sha1Name, String message) {
        try {
            Ref.createSha1Ref(sha1Name);
            fail("IllegalArgumentException not thrown when one was expected!");
        } catch (IllegalArgumentException e) {
            assertEquals("IllegalArgumentException did not contain the expected message.", message, e
                    .getMessage());
        }
    }

    @Test
    public void testCreateTagRef() {
        // Testing invalid input
        assertCreateTagRefThrowsNPE(null,
                "000001: A String argument was not specified but is required.  { variableName=[name] }");
        assertCreateTagRefThrowsIllegalArgException("",
                "000001: A String argument was not specified but is required.  { variableName=[name] }");

        // Testing valid input
        Ref ref = Ref.createTagRef("v2.1");
        Ref ref2 = Ref.createTagRef("v2.1");
        assertEquals("Expected RefType of TAG.", ref.getRefType(), Ref.RefType.TAG);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "v2.1");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "v2.1");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());

        ref = Ref.createTagRef("Release-5.5.2");
        assertFalse(ref.equals(ref2));

        ref2 = Ref.createTagRef("Release-5.5.2");
        assertEquals("Expected RefType of TAG.", ref.getRefType(), Ref.RefType.TAG);
        assertEquals(ref.getHeadOffset(), -1);
        assertEquals(ref.getName(), "Release-5.5.2");
        assertNull(ref.getRepositoryName());
        assertEquals(ref.toString(), "Release-5.5.2");
        assertEquals(ref, ref2);
        assertEquals(ref.hashCode(), ref2.hashCode());
    }

    private void assertCreateTagRefThrowsNPE(String name, String message) {
        try {
            Ref.createTagRef(name);
            fail("NPE exception not thrown when one was expected!");
        } catch (NullPointerException e) {
            assertEquals("NPE did not contain the expected message.", message, e.getMessage());
        }
    }

    private void assertCreateTagRefThrowsIllegalArgException(String name, String message) {
        try {
            Ref.createTagRef(name);
            fail("IllegalArgumentException not thrown when one was expected!");
        } catch (IllegalArgumentException e) {
            assertEquals("IllegalArgumentException did not contain the expected message.", message, e
                    .getMessage());
        }
    }

}
