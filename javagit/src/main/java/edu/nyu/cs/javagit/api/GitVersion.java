package edu.nyu.cs.javagit.api;

import java.io.File;
import java.util.Collection;

import edu.nyu.cs.javagit.api.commands.CommandResponse;
import edu.nyu.cs.javagit.client.cli.IParser;
import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.utilities.ExceptionMessageMap;

/**
 * Git version is composed by major.minor.releaseMajor{"." + releaseMinor|"-" + tag}  meaning that
 * release minor and tag are optional and mutually excluded.
 * <br/>
 * These rules are valid for Git v1.1.0 onwards. This object is not intended to manage Git versions previous to this one
 * <p/>
 * Valid values are : 1.7.0, 1.8.0.1 and 1.8.2-rc4 among others
 */
public class GitVersion
        implements CommandResponse, Comparable<GitVersion> {

    public static final int LATER = +1;
    public static final int PREVIOUS = -1;
    public static final int SAME = 0;
    private final Validator validator;

    private String tag;
    private Integer releaseMinor = null;
    private int releaseMajor;
    private int major;
    private int minor;
    private String version;

    /**
     * Constructor.
     *
     * @param validator
     * @param version   The version of git being used.
     */
    public GitVersion(Validator validator, String version) throws JavaGitException {
        this.validator = validator;
        this.version = avoidSpacesIn(version);
        this.validator.checkStringArgument(this.version, "version");
        String[] versionNumber = this.version.split("\\.", 4);
        this.major = parseNumber(versionNumber[0], 442001);
        if (versionNumber.length > 1) {
            this.minor = parseNumber(versionNumber[1], 442002);
            if (versionNumber.length > 2) {
                String[] relDashSplit = versionNumber[2].split("-", 2);
                if (relDashSplit.length == 2) {
                    this.releaseMajor = parseNumber(relDashSplit[0], 442003);
                    this.tag = relDashSplit[1];
                } else if (versionNumber.length == 4) {
                    this.releaseMajor = parseNumber(versionNumber[2], 442003);
                    this.releaseMinor = parseNumber(versionNumber[3], 442004);
                } else {
                    this.releaseMajor = parseNumber(versionNumber[2], 442003);
                }
            }
        }

    }

    private void reportError(int i) throws JavaGitException {
        throw new JavaGitException(i, ExceptionMessageMap.getMessage(i));
    }

    private String avoidSpacesIn(String version) {
        String returnVersion = version.trim();
        final int indexOf = returnVersion.indexOf(" ");
        if (indexOf != -1) {
            returnVersion = returnVersion.substring(0, indexOf);
        }

        return returnVersion;
    }

    private int parseNumber(String s, int code) throws JavaGitException {
        int returnVersion = 0;
        try {
            returnVersion = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            reportError(code);
        }
        return returnVersion;
    }

    /**
     * Gets the version of git being used in <code>String</code> format.
     *
     * @return The git version string.
     */
    @Override
    public String toString() {
        return version;
    }

    public int getMinor() {
        return minor;
    }

    public int getMajor() {
        return major;
    }

    public int getReleaseMajor() {
        return releaseMajor;
    }

    public String getTag() {
        return tag;
    }

    public int getReleaseMinor() {
        return releaseMinor;
    }

    public boolean containsReleaseMinor() {
        return releaseMinor != null;
    }

    public boolean containsTag() {
        return getTag() != null;
    }

    public boolean equals(Object that) {
        if (!(that instanceof GitVersion))
            return super.equals(that);

        return( this.version.equals( ((GitVersion)that).version) );
    }

    public int compareTo(GitVersion that) {

        if (this.getMajor() > that.getMajor()) {
            return LATER;
        } else if (this.getMajor() < that.getMajor()) {
            return PREVIOUS;
        } else {
            if (this.getMinor() > that.getMinor()) {
                return LATER;
            } else if (this.getMinor() < that.getMinor()) {
                return PREVIOUS;
            } else {
                if (this.getReleaseMajor() > that.getReleaseMajor()) {
                    return LATER;
                } else if (this.getReleaseMajor() < that.getReleaseMajor()) {
                    return PREVIOUS;
                } else {
                    return compareToReleaseMinorAndTag(that);
                }
            }
        }
    }

    private int compareToReleaseMinorAndTag(GitVersion that) {
        if (!this.containsReleaseMinor() && !that.containsReleaseMinor())
            return compareToTag(that);
        else if (!this.containsTag() && !that.containsTag())
            return compareToReleaseMinor(that);
        else if (this.containsReleaseMinor() && !that.containsTag())
            return LATER;
        else
            return PREVIOUS;
    }

    /**
     * Compares minor releases values given that git version contains no tag
     * for none of GitVersion objects
     *
     * @param that
     * @return  SAME if both are equal, LATER if that is previous to this or PREVIOUS otherwise
     */
    private int compareToReleaseMinor(GitVersion that) {
        if (this.containsReleaseMinor() && that.containsReleaseMinor())
            return compareToInt(this.getReleaseMinor(), that.getReleaseMinor());
        else if (!this.containsReleaseMinor() && !that.containsReleaseMinor())
            return SAME;
        else if (this.containsReleaseMinor() && !that.containsReleaseMinor())
            return LATER;
        else
            return PREVIOUS;
    }

    /**
     * Compares tag values given that git version contains no minor release
     * for none of GitVersion objects
     * <p/>
     * When both contain a tag or not contain  it the comparison is easy but
     * if only one of them contains it the release that doesn't contains it
     * is the one released earlier
     * <p/>
     * e.g. v1.8.0-rc0 is previous to v1.8.0
     *
     * @param that
     * @return SAME if both are equal, LATER if that is previous to this or PREVIOUS otherwise
     */
    private int compareToTag(GitVersion that) {
        if (this.containsTag() && that.containsTag())
            return this.getTag().compareTo(that.getTag());
        else if (!this.containsTag() && !that.containsTag())
            return SAME;
        else if (this.containsTag() && !that.containsTag())
            return PREVIOUS;
        else
            return LATER;
    }

    private int compareToInt(int iThis, int iThat) {
        return Integer.compare(iThis, iThat);

    }

    /*
     * <code>GitVersionParser</code> parses the output of the <code>git --version</code> command.
     * It is also used to determine if the git binaries are accessible via the command line.
     */
    static class GitVersionParser implements IParser {
        // The version of git that we parse out.
        private String version = "";

        // Whether or not we saw a valid version string.
        private boolean parsedCorrectly = true;

        // We only care about parsing the first line of our input - watch that here.
        private boolean sawLine = false;
        private Validator validator;

        GitVersionParser(Validator validator) {
            this.validator = validator;
        }

		/**
		 * Returns the <code>GitVersion</code> object that's essentially just a wrapper around
		 * the git version number.
		 *
		 * @param output
		 * @return The response object containing the version number.
		 */
		public CommandResponse getResponse(Collection<String> output) throws JavaGitException {
			if (!(parsedCorrectly)) {
				throw new JavaGitException(100001, ExceptionMessageMap.getMessage("100001"));
			}
			return new GitVersion(validator, version);
		}

        @Override
        public void setWorkingDir(File workingDir) {
            throw new UnsupportedOperationException();
        }

        /**
         * Parses the output of <code>git --version</code>. Expects to see: "git version XYZ".
         *
         * @param line <code>String</code> containing the line to be parsed.
         */
        public void parseLine(String line) {
            if (!(sawLine)) {
                sawLine = true;
                parsedCorrectly = (line.trim().indexOf("git version ") == 0);
                if (parsedCorrectly) {
                    version = line.replaceAll("git version ", "");
                }
            }
        }

        public void processExitCode(int code) {
        }

    }
}
