package edu.nyu.cs.javagit.client.cli;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface Validator {
	void checkNullArgument(File file, String message);

	void checkFileValidity(File file) throws IOException;

	boolean checkUnorderedListsEqual(List<?> l1, List<?> l2);

	void checkStringArgument(String str, String variableName);
}
