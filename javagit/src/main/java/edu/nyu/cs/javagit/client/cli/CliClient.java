/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.client.cli;

import edu.nyu.cs.javagit.api.Configuration;
import edu.nyu.cs.javagit.api.commands.CommandResponse;
import edu.nyu.cs.javagit.client.IClient;
import edu.nyu.cs.javagit.client.IGitAdd;
import edu.nyu.cs.javagit.client.IGitBranch;
import edu.nyu.cs.javagit.client.IGitCheckout;
import edu.nyu.cs.javagit.client.IGitClone;
import edu.nyu.cs.javagit.client.IGitCommit;
import edu.nyu.cs.javagit.client.IGitDiff;
import edu.nyu.cs.javagit.client.IGitFetch;
import edu.nyu.cs.javagit.client.IGitGrep;
import edu.nyu.cs.javagit.client.IGitInit;
import edu.nyu.cs.javagit.client.IGitLog;
import edu.nyu.cs.javagit.client.IGitMv;
import edu.nyu.cs.javagit.client.IGitPull;
import edu.nyu.cs.javagit.client.IGitReset;
import edu.nyu.cs.javagit.client.IGitRevert;
import edu.nyu.cs.javagit.client.IGitRm;
import edu.nyu.cs.javagit.client.IGitShow;
import edu.nyu.cs.javagit.client.IGitStatus;
import edu.nyu.cs.javagit.utilities.UrlMapper;

public class CliClient
        implements IClient {

  private Configuration configuration;
  private Validator validator;
  private GitProcessBuilder processBuilder;
  private CommandRunner<CommandResponse> commandRunner;

  public CliClient(Configuration configuration) {
    this.configuration = configuration;
    this.validator = new ValidatorImpl();
    this.processBuilder = new GitProcessBuilder();
    this.commandRunner = new CommandRunner<>();
  }

  public IGitAdd getGitAddInstance() {
    return new CliGitAdd(configuration);
  }

  public IGitCommit getGitCommitInstance() {
    return new CliGitCommit(configuration, validator);
  }

  public IGitDiff getGitDiffInstance() {
    return new CliGitDiff();
  }

  public IGitGrep getGitGrepInstance() {
    return new CliGitGrep();
  }

  public IGitLog getGitLogInstance() {
    return new CliGitLog(configuration);
  }

  public IGitMv getGitMvInstance() {
    return new CliGitMv(configuration);
  }

  public IGitReset getGitResetInstance() {
    return new CliGitReset(configuration);
  }

  public IGitRevert getGitRevertInstance() {
    return new CliGitRevert();
  }

  public IGitRm getGitRmInstance() {
    return new CliGitRm(configuration);
  }

  public IGitShow getGitShowInstance() {
    return new CliGitShow();
  }

  public IGitStatus getGitStatusInstance() {
    return new CliGitStatus(configuration, validator, processBuilder, commandRunner);
  }

  public IGitBranch getGitBranchInstance() {
    return new CliGitBranch(configuration, validator, processBuilder, commandRunner);
  }

  public IGitCheckout getGitCheckoutInstance() {
    return new CliGitCheckout(configuration);
  }

  public IGitInit getGitInitInstance() {
    return new CliGitInit(configuration);
  }

  public IGitClone getGitCloneInstance() {
    return new CliGitClone(configuration, new UrlMapper());
  }

  public IGitFetch getGitFetchInstance() {
    return new CliGitFetch(configuration);
  }

  @Override
  public IGitPull getGitPullInstance()
  {
    return new CliGitPull();
  }


}
