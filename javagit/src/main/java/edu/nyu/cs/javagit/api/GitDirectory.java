/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api;

import edu.nyu.cs.javagit.api.commands.GitLog;
import edu.nyu.cs.javagit.api.commands.GitLogOptions;
import edu.nyu.cs.javagit.api.commands.GitLogResponse.Commit;
import edu.nyu.cs.javagit.client.cli.Validator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <code>GitDirectory</code> represents a directory object in a git working tree.
 */
public class GitDirectory
        extends GitFileSystemObject {

    /**
     * The constructor.  Both arguments are required (i.e. cannot be null).
     *
     * @param dir           The underlying {@link File} object that we want to augment with git
     *                      functionality.
     * @param workingTree
     * @param configuration
     * @param validator
     */
    protected GitDirectory(File dir, WorkingTree workingTree, Configuration configuration,
                           Validator validator)
            throws JavaGitException {
        super(configuration, validator, dir, workingTree);
    }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof GitDirectory)) {
      return false;
    }

    GitFileSystemObject gitObj = (GitFileSystemObject) obj;
    return super.equals(gitObj);
  }

  /**
   * Gets the children of this directory.
   * 
   * @return The children of this directory.
   */
  public List<GitFileSystemObject> getChildren() throws IOException, JavaGitException {
    List<GitFileSystemObject> children = new ArrayList<GitFileSystemObject>();

    // get all of the file system objects currently located under this directory
    for (File memberFile : file.listFiles()) {
      // check if this file is hidden also some times the .git and 
      //other unix hidden directories are not hidden in Windows
      if (memberFile.isHidden()||memberFile.getName().startsWith(".")) {
        // ignore (could be .git directory)
        continue;
      }

      // now, just check for the type of the filesystem object
      if (memberFile.isDirectory()) {
          children.add(new GitDirectory(memberFile, workingTree, configuration, validator));
      } else {
          children.add(new GitFile(memberFile, workingTree, configuration, validator));
      }
    }

    return children;
  }
  
	/**
	 * Show commit logs
	 * 
	 * @return List of commits for the working directory
	 * @throws IOException 
	 * @throws JavaGitException 
	 */
	public List<Commit> getLog() throws JavaGitException, IOException {
        GitLog gitLog = new GitLog(configuration, validator);
        return gitLog.log(this.file);
	}

	/**
	 * 
	 * @param options	Options to the git log command
	 * @return	List of commits for the working directory
	 * @throws JavaGitException
	 * @throws IOException
	 */
	public List<Commit> getLog(GitLogOptions options) throws JavaGitException, IOException {
        GitLog gitLog = new GitLog(configuration, validator);
        return gitLog.log(this.file,options);
	}
}
