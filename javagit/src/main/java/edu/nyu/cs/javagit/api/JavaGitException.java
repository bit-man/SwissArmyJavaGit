/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api;

import java.util.ArrayList;
import java.util.Collection;

import edu.nyu.cs.javagit.utilities.ExceptionMessageMap;

/**
 * Base exception for git specific exceptions.
 */
public class JavaGitException extends Exception {
	private static final long serialVersionUID = 1402053559415331074L;
	private Collection<String> output = new ArrayList<>();
	private int code;

	public JavaGitException(int code) {
		super(ExceptionMessageMap.getMessage(code));
		this.code = code;
	}

	public JavaGitException(int code, Collection<String> output) {
		super(ExceptionMessageMap.getMessage(code));
		this.code = code;
		this.output = output;
	}

	public JavaGitException(int code, String message) {
		super(message);
		this.code = code;
	}

	public JavaGitException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public JavaGitException(int code, String message, Collection<String> output) {
		super(message);
		this.code = code;
		this.output = output;
	}

	public JavaGitException(int code, Throwable cause, Collection<String> output) {
		super(ExceptionMessageMap.getMessage(code), cause);
		this.output = output;
	}

	public int getCode() {
		return code;
	}

	public Collection<String> getOutput() {
		return output;
	}
}
