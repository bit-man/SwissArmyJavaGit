/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import edu.nyu.cs.javagit.api.commands.GitBranch;
import edu.nyu.cs.javagit.api.commands.GitBranchOptions;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse;
import edu.nyu.cs.javagit.api.commands.GitLog;
import edu.nyu.cs.javagit.api.commands.GitLogOptions;
import edu.nyu.cs.javagit.api.commands.GitLogResponse.Commit;
import edu.nyu.cs.javagit.api.commands.GitPull;
import edu.nyu.cs.javagit.client.cli.Validator;

/**
 * The <code>DotGit</code> represents the .git directory.
 */
public final class DotGit {

  // The directory that contains the .git in question.
  private final File path;

    private Configuration configuration;
    private Validator validator;

    /**
     * The constructor. Private because this singleton-ish (per each repository) class is only
     * available via the getInstance method.
     *
     * @param path          The path to the directory containing the .git file in question.
     * @param configuration
     * @param validator
     */
    public DotGit(File path, Configuration configuration, Validator validator) {
        this.path = path;
        this.configuration = configuration;
        this.validator = validator;
    }

  /**
   * Since instances of this class are singletons, don't allow cloning.
   * 
   * @return None - always throws exception
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
  }

  /**
   * Creates a new branch
   * 
   * @param name
   *          The name of the branch to create
   * 
   * @return The new branch
   */
  public Ref createBranch(String name) throws IOException, JavaGitException {
	  Ref newBranch = Ref.createBranchRef(name);
	  GitBranch gitBranch = new GitBranch(configuration);
    gitBranch.createBranch(path, newBranch);
    return newBranch;
  }

  /**
   * Deletes a branch
   * 
   * @param branch
   *          The branch to delete
   * @param forceDelete
   *          True if force delete option -D should be used, 
   *          false if -d should be used.
   * @throws IOException
   *          Thrown in case of I/O operation failure
   * @throws JavaGitException
   *          Thrown when there is an error executing git-branch.
   */
  public void deleteBranch(Ref branch, boolean forceDelete) 
    throws IOException, JavaGitException {
	  GitBranch gitBranch = new GitBranch(configuration);
    gitBranch.deleteBranch(path, forceDelete, false, branch);
  }
  
  /**
   * Renames a branch
   * 
   * @param branchFrom
   *          The branch to rename
   * @param nameTo
   *          New branch name
   * @param forceRename
   *          True if force rename option -M should be used. 
   *          False if -m should be used. 
   * @return new <code>Ref</code> instance
   * @throws IOException
   *          Thrown in case of I/O operation failure
   * @throws JavaGitException
   *          Thrown when there is an error executing git-branch.
   */
  public Ref renameBranch(Ref branchFrom, String nameTo, boolean forceRename) 
    throws IOException, JavaGitException {
	  Ref newBranch = Ref.createBranchRef(nameTo);
	  GitBranch gitBranch = new GitBranch(configuration);
    gitBranch.renameBranch(path, forceRename, branchFrom, newBranch);
    return newBranch;
  }

  /**
   * Gets a list of the branches in the repository.
   * 
   * @return The branches in the repository.
   */
  public Iterator<Ref> getBranches() throws IOException, JavaGitException {
	  GitBranch gitBranch = new GitBranch(configuration);
	  GitBranchOptions options = new GitBranchOptions();
    GitBranchResponse response = gitBranch.branch(path, options);
    return response.getBranchListIterator();
  }
  
  /**
   * Gets the repository path represented by this repository object.
   * 
   * @return The path to the repository
   */
  public File getPath() {
    return path;
  }

    /**
     * Gets the working tree for this git repository
     *
     * @return The working tree
     */
    public WorkingTree getWorkingTree() {
        return new WorkingTree(path, configuration, validator);
  }

    /**
     * Fetch from git repository and merge with the current one
     *
     * @param gitFrom The git repository being fetched from
     */
    public void pull(DotGit gitFrom)
            throws JavaGitException {
        new GitPull(configuration).pull(gitFrom.getPath());
    }

	/**
	 * Show commit logs
	 * 
	 * @return List of commits for the working directory
	 * @throws IOException 
	 * @throws JavaGitException 
	 */
	public List<Commit> getLog() throws JavaGitException, IOException {
        GitLog gitLog = new GitLog(configuration, validator);
        return gitLog.log(this.getPath());
	}

	/**
	 * 
	 * @param options	Options to the git log command
	 * @return	List of commits for the working directory
	 * @throws JavaGitException
	 * @throws IOException
	 */
	public List<Commit> getLog(GitLogOptions options) throws JavaGitException, IOException {
        GitLog gitLog = new GitLog(configuration, validator);
        return gitLog.log(this.getPath(),options);
	}

}
