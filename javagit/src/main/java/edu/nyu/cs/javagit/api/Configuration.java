package edu.nyu.cs.javagit.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.nyu.cs.javagit.client.cli.CommandRunner;
import edu.nyu.cs.javagit.client.cli.GitProcessBuilder;
import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.utilities.CheckUtilities;
import edu.nyu.cs.javagit.utilities.ExceptionMessageMap;

public class Configuration {
    private File gitPath;
    private GitVersion gitVersion = null;
    private Validator validator;

    public Configuration(File gitPath, Validator validator) throws IOException, JavaGitException {
        this.validator = validator;
        setGitPath(gitPath);
    }

    public Configuration(String gitPath, Validator validator) throws IOException, JavaGitException {
        this.validator = validator;
        setGitPath(gitPath);
    }

    public void setGitPath(String path)
            throws IOException, JavaGitException {
        CheckUtilities.checkStringArgument(path, "path");
        setGitPath(new File(path));
    }

    private void setGitPath(File path)
            throws IOException, JavaGitException {
        if (path != null) {
            CheckUtilities.checkFileValidity(path);

            if (!(path.isDirectory())) {
                throw new JavaGitException(020002, ExceptionMessageMap.getMessage("020002") + " { path=["
                        + path.getPath() + "] }");
            }
        }

        try {
            determineGitVersion(path);
        } catch (Exception e) {
            // The path that was passed in doesn't work. Catch any errors and throw this one instead.
            throw new JavaGitException(100002, ExceptionMessageMap.getMessage("100002") + " { path=["
                    + path.getPath() + "] }", e);
        }

        // Make sure we're hanging onto an absolute path.
        gitPath = (path != null) ? path.getAbsoluteFile() : null;
    }

    public String getGitCommandPrefix() {
        return ((gitPath == null) ? "" : (gitPath.getAbsolutePath() + File.separator));
    }

    private void determineGitVersion(File path)
            throws JavaGitException {

        /*
         * If they already set the path explicitly, or are in the process of doing so (via the path
         * argument), make sure to prefix the git call with it. If they didn't, assume it's blank.
         */
        String gitPrefix = "";
        if (path != null) {
            // We got a path passed in as an argument.
            gitPrefix = path.getAbsolutePath() + File.separator;
        } else if (gitPath != null) {
            // They didn't pass in a path, but previously set it explicitly via setGitPath.
            gitPrefix = getGitCommandPrefix();
        }

        String gitCommand = gitPrefix + getGitBinaryPath();
        if (!(gitPrefix.equals(""))) {
            // If we've got a full path to the git binary here, ensure it actually exists.
            if (!(new File(gitCommand).exists())) {
                throw new JavaGitException(100002, ExceptionMessageMap.getMessage("100002"));
            }
        }

        List<String> commandLine = new ArrayList<String>();
        commandLine.add(gitCommand);
        commandLine.add("--version");

        // Now run the actual git version command.
        try {
            gitVersion = new CommandRunner<GitVersion>(new GitVersion.GitVersionParser(validator), new GitProcessBuilder(commandLine)).run();
        } catch (Exception e) {
            throw new JavaGitException(100001, ExceptionMessageMap.getMessage("100001"));
        }

        String version = gitVersion.toString();
        if (!(isValidVersionString(version))) {
            throw new JavaGitException(100001, ExceptionMessageMap.getMessage("100001"));
        }

    }

    private boolean isValidVersionString(String version) {
        /*
         * Git version strings can vary, so let's do a minimal sanity check for two things: 1. The first
         * character in the version is a number. 2. There's at least one period in the version string.
         *
         * TODO (rs2705): Make this more sophisticated by parsing out a major/minor version number, and
         * ensuring it's >= some minimally-required version.
         */
        try {
            Integer.parseInt(version.substring(0, 1));
        } catch (NumberFormatException e) {
            // First character in the version string was not a valid number!
            return false;
        }

        return version.contains(".");
    }

    private String getGitBinaryPath() {
        return System.getProperty("os.name").contains("indows") ? "git.exe" : "git";
    }

    public String getGitCommand() {
        return getGitCommandPrefix() + getGitBinaryPath();
    }

    public GitVersion getGitVersionObject()
            throws JavaGitException {
        if (gitVersion == null) {
            determineGitVersion(gitPath);
        }

        return gitVersion;
    }

    public String getGitVersion()
            throws JavaGitException {
        return getGitVersionObject().toString();
    }
}
