/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.nyu.cs.javagit.api.commands.GitAddResponse;
import edu.nyu.cs.javagit.api.commands.GitBranch;
import edu.nyu.cs.javagit.api.commands.GitBranchOptions;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse;
import edu.nyu.cs.javagit.api.commands.GitCheckout;
import edu.nyu.cs.javagit.api.commands.GitCommit;
import edu.nyu.cs.javagit.api.commands.GitCommitResponse;
import edu.nyu.cs.javagit.api.commands.GitStatus;
import edu.nyu.cs.javagit.api.commands.GitStatusResponse;
import edu.nyu.cs.javagit.client.cli.Validator;

/**
 * The <code>WorkingTree</code> represents the working copy of the files in the current branch.
 */
public final class WorkingTree {
    // The directory that contains the .git in question.
  private final File path;

  // A git-specific representation of the same place this class is pointing.
  private GitDirectory rootDir;
    private Configuration configuration;
    private Validator validator;

    /**
     * The constructor. Private because this singleton-ish (per each repository) class is only
     * available via the getInstance method.
     *
     * @param path          The path to the working directory represented by the instance being created.
     * @param configuration : Git configuration
     * @param validator
     */
    public WorkingTree(File path, Configuration configuration, Validator validator) {
        this.path = path;
        this.configuration = configuration;
        this.validator = validator;
        try {
            this.rootDir = new GitDirectory(path, this, configuration, validator);
        } catch (JavaGitException e) {
            //that is really impossible
        }
    }

    /**
   * Adds all known and modified files in the working directory to the index.
   * 
   * @return response from git add
   */
  public GitAddResponse add() throws IOException, JavaGitException {
    return rootDir.add();
  }

  /**
   * Adds a directory to the working directory (but not to the repository!)
   * 
   * @param dir
   *          name of the directory
   * 
   * @return The new <code>GitDirectory</code> object
   * 
   * @throws JavaGitException
   *         File path specified does not belong to git repo/ working tree
   */
  public GitDirectory addDirectory(String dir) throws JavaGitException {
      return new GitDirectory(new File(dir), this, configuration, validator);
  }

  /**
   * Since instances of this class are singletons, don't allow cloning.
   * 
   * @return None - always throws exception
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
  }

  /**
   * Commits the objects specified in the index to the repository.
   * 
   * @param comment
   *          Developer's comment about the change
   * 
   * @return response from git commit
   */
  public GitCommitResponse commit(String comment) throws IOException, JavaGitException {
    GitCommit gitCommit = new GitCommit(configuration, validator);
      return gitCommit.commit(path, comment);
  }

  /**
   * Automatically stage files that have been modified and deleted, but new files you have not 
   * told git about are not affected
   * 
   * @param comment
   *          Developer's comment about the change
   * 
   * @return response from git commit
   */
  public GitCommitResponse commitAll(String comment) throws IOException, JavaGitException {
    GitCommit gitCommit = new GitCommit(configuration, validator);
      return gitCommit.commitAll(path, comment);
  }

  /**
   * Stage all files and commit (including untracked)
   * 
   * @param comment
   *          Developer's comment about the change
   * @return <code>GitCommitResponse</code> object
   * @throws IOException
   *          I/O operation fails
   * @throws JavaGitException
   *          git command fails
   */
  public GitCommitResponse addAndCommitAll(String comment) throws IOException, JavaGitException {
    return rootDir.commit(comment);
  }

  /**
   * Gets the currently checked-out branch of the working directory.
   * 
   * @return The currently checked-out branch of the working directory.
   */
  public Ref getCurrentBranch() throws IOException, JavaGitException {
	  GitBranch gitBranch = new GitBranch(configuration);
	  GitBranchOptions options = new GitBranchOptions();
      GitBranchResponse response = gitBranch.branch(path, options);
    return response.getCurrentBranch();
  }

  /**
   * Take a standard <code>File</code> object and return it wrapped in a <code>GitDirectory</code>.
   * 
   * @return A new <code>GitDirectory</code> object representing the given <code>File</code>.
   * 
   * @throws JavaGitException
   *         File path specified does not belong to git repo/ working tree
   */
  public GitDirectory getDirectory(File file) throws JavaGitException {
      return new GitDirectory(file, this, configuration, validator);
  }

  /**
   * Take a standard <code>File</code object and return it wrapped in a <code>GitFile</code>.
   * 
   * @return A new <code>GitFile</code> object representing the given <code>File</code>.
   * 
   * @throws JavaGitException
   *         File path specified does not belong to git repo/ working tree
   */
  public GitFile getFile(File file) throws JavaGitException {
      return new GitFile(file, this, configuration, validator);
  }

  /**
   * Show commit logs
   * 
   * @return List of commits for the working directory
   */
  public List<Commit> getLog() {
    // TODO (ma1683): Implement this method
    return null;
  }

    /**
     * Gets the .git representation for this git repository
     *
     * @return The DotGit
     */
    public DotGit getDotGit() {
        return new DotGit(path, configuration, validator);
  }

  /**
   * Gets the path to the working directory represented by an instance.
   * 
   * @return The path to the working directory represented by an instance.
   */
  public File getPath() {
    return path;
  }

  /**
   * Gets the filesystem tree; equivalent to git-status
   * 
   * @return The list of objects at the root directory
   * 
   * @throws JavaGitException
   *         File path specified does not belong to git repo/ working tree
   */
  public List<GitFileSystemObject> getTree() throws IOException, JavaGitException {
    // TODO (rs2705): Make this work - will throw NullPointerException
      return new GitDirectory(path, this, configuration, validator).getChildren();
  }

  /**
   * Reverts the specified git commit
   * 
   * @param commit
   *          Git commit that user wishes to revert
   */
  public void revert(Commit commit) {
    // TODO (ma1683): Implement this method
    // GitRevert.revert(commit.getSHA1());
  }

  /**
   * Switches to a new branch
   * 
   * @param ref
   *          Git branch/sha1 to switch to
   */
  public void checkout(Ref ref) throws IOException, JavaGitException {
    GitCheckout gitCheckout = new GitCheckout(configuration, validator);
      gitCheckout.checkout(path, null, ref);

    /*
     * TODO (rs2705): Figure out why this function is setting this.path. When does the WorkingTree
     * path change?
     */
    // this.path = branch.getBranchRoot().getPath();
  }
  

  /**
   * Gets the status of all files in the working directory
   * 
   * @return <code>GitStatusResponse</code> object
   * @throws IOException
   *         Exception is thrown if any of the IO operations fail.
   * @throws JavaGitException
   *         Exception thrown if the repositoryPath is null
   */
  public GitStatusResponse getStatus() throws IOException, JavaGitException {
      GitStatus gitStatus = new GitStatus(configuration, validator);
      return gitStatus.status(path);
  }

}