/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api.commands;

import edu.nyu.cs.javagit.api.Ref;
import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.utilities.CheckUtilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A response data object for the git-branch command.
 */
abstract public class GitBranchResponse
        implements CommandResponse {
  protected Validator validator;

  public enum ResponseType {
    BRANCH_LIST, MESSAGE
  }

  protected List<Ref> branchList;

  protected List<BranchRecord> listOfBranchRecord;

  protected StringBuffer messages = new StringBuffer();

  protected Ref currentBranch;

  protected ResponseType responseType;

  public GitBranchResponse(Validator validator) {
    this.validator = validator;
    branchList = new ArrayList<>();
    listOfBranchRecord = new ArrayList<>();
  }

  public boolean equals(Object o) {
    if (!(o instanceof GitBranchResponse)) {
      return false;
    }

    GitBranchResponse g = (GitBranchResponse) o;

    if (!CheckUtilities.checkObjectsEqual(getResponseType(), g.getResponseType())) {
      System.out.println("Not Equal getResponseType");
      return false;
    }

    if (!CheckUtilities.checkObjectsEqual(getMessages(), g.getMessages())) {
      System.out.println("Not Equal getMessages");
      return false;
    }

    if (!CheckUtilities.checkObjectsEqual(getCurrentBranch(), g.getCurrentBranch())) {
      return false;
    }

    if (!validator.checkUnorderedListsEqual(branchList, g.branchList)) {
      System.out.println("Not Equal branchList");
      return false;
    }

    if (!validator.checkUnorderedListsEqual(listOfBranchRecord, g.listOfBranchRecord)) {
      System.out.println("Not Equal listOfBranchRecord");
      return false;
    }

    return true;
  }

  public Iterator<Ref> getBranchListIterator() {
    return (new ArrayList<>(branchList).iterator());
  }

  public Iterator<BranchRecord> getListOfBranchRecordIterator() {
    return (new ArrayList<>(listOfBranchRecord).iterator());
  }

  public ResponseType getResponseType() {
    return responseType;
  }

  public String getMessages() {
    return messages.toString();
  }

  public Ref getCurrentBranch() {
    return currentBranch;
  }

  public int hashCode() {
    return branchList.hashCode() + listOfBranchRecord.hashCode();
  }

  public static class BranchRecord {
    private Ref branch;

    // The SHA Refs of a branch in the response of git-branch with -v option.
    private Ref sha1;

    // String Buffer to store the comment after execution of git-branch command with -v option.
    private String comment;

    // Variable to store the current branch.
    private boolean isCurrentBranch;

    public BranchRecord(Ref branch, Ref sha1, String comment, boolean isCurrentBranch) {
      this.branch = branch;
      this.sha1 = sha1;
      this.comment = comment;
      this.isCurrentBranch = isCurrentBranch;
    }

    public boolean equals(Object o) {
      if (!(o instanceof BranchRecord)) {
        return false;
      }

      BranchRecord c = (BranchRecord) o;

      if (!CheckUtilities.checkObjectsEqual(getBranch(), c.getBranch())) {
        return false;
      }

      if (!CheckUtilities.checkObjectsEqual(getSha1(), c.getSha1())) {

        return false;
      }

      if (!CheckUtilities.checkObjectsEqual(getComment(), c.getComment())) {
        return false;
      }

      if (isCurrentBranch() != c.isCurrentBranch()) {
        return false;
      }

      return true;
    }

    public Ref getBranch() {
      return branch;
    }

    public Ref getSha1() {
      return sha1;
    }

    public String getComment() {
      return comment;
    }

    public int hashCode() {
      return branch.hashCode() + sha1.hashCode() + comment.hashCode();
    }

    public boolean isCurrentBranch() {
      return isCurrentBranch;
    }
  }
}
