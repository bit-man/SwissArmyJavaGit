package edu.nyu.cs.javagit.client.parser;

import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.commands.CommandResponse;
import edu.nyu.cs.javagit.api.commands.VoidResponse;
import edu.nyu.cs.javagit.client.cli.IParser;

import java.io.File;
import java.util.Collection;

public class GitBranchCreateParser
        implements IParser {

  private int exitCode;

  @Override
  public void parseLine(String line) {
  }

  @Override
  public void processExitCode(int code) {
    this.exitCode = code;
  }

  @Override
  public CommandResponse getResponse(Collection<String> output)
          throws JavaGitException {
    if (exitCode != 0) {
      throw new JavaGitException(404000, output);
    }

    return new VoidResponse();
  }

  @Override
  public void setWorkingDir(File workingDir) {
  }
}
