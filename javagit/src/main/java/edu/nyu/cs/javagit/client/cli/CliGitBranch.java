/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.client.cli;

import edu.nyu.cs.javagit.api.Configuration;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.Ref;
import edu.nyu.cs.javagit.api.commands.CommandResponse;
import edu.nyu.cs.javagit.api.commands.GitBranchOptions;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse.BranchRecord;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse.ResponseType;
import edu.nyu.cs.javagit.api.commands.VoidResponse;
import edu.nyu.cs.javagit.client.GitBranchResponseImpl;
import edu.nyu.cs.javagit.client.IGitBranch;
import edu.nyu.cs.javagit.client.parser.GitBranchCreateParser;
import edu.nyu.cs.javagit.utilities.CheckUtilities;
import edu.nyu.cs.javagit.utilities.ExceptionMessageMap;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;

public class CliGitBranch
        implements IGitBranch {
  private Configuration configuration;
  private Validator validator;
  private IGitProcessBuilder processBuilder;
  private ICommandRunner<CommandResponse> commandRunner;

  public CliGitBranch(Configuration configuration, Validator validator,
                      IGitProcessBuilder processBuilder,
                      ICommandRunner<CommandResponse> commandRunner) {
    this.configuration = configuration;
    this.validator = validator;
    this.processBuilder = processBuilder;
    this.commandRunner = commandRunner;
  }

  public GitBranchResponse branch(File repoPath)
          throws IOException, JavaGitException {
    return branchCommandExecute(repoPath, null, null, null, null);
  }

  public GitBranchResponseImpl branch(File repoPath, GitBranchOptions options)
          throws IOException,
          JavaGitException {
    return branchCommandExecute(repoPath, options, null, null, null);
    }

  public GitBranchResponseImpl deleteBranch(File repoPath, boolean forceDelete, boolean remote,
      Ref branchName) throws IOException, JavaGitException {
    GitBranchOptions options = new GitBranchOptions();
    setDeleteOptions(options, forceDelete, remote);
    return branchCommandExecute(repoPath, options, branchName, null, null);
  }

  public GitBranchResponseImpl deleteBranches(File repoPath, boolean forceDelete, boolean remote,
      List<Ref> branchList) throws IOException, JavaGitException {
    GitBranchOptions options = new GitBranchOptions();
    setDeleteOptions(options, forceDelete, remote);
    return branchCommandExecute(repoPath, options, null, null, branchList);
  }

  public GitBranchResponseImpl renameBranch(File repoPath, boolean forceRename, Ref newName)
      throws IOException, JavaGitException {
    GitBranchOptions options = new GitBranchOptions();
    if (forceRename) {
      options.setOptMUpper(true);
    } else {
      options.setOptMLower(true);
    }
    return branchCommandExecute(repoPath, options, newName, null, null);
  }

  public GitBranchResponseImpl renameBranch(File repoPath, boolean forceRename, Ref oldName, Ref newName)
      throws IOException, JavaGitException {
    GitBranchOptions options = new GitBranchOptions();
    if (forceRename) {
      options.setOptMUpper(true);
    } else {
      options.setOptMLower(true);
    }
    return branchCommandExecute(repoPath, options, oldName, newName, null);
  }

  public VoidResponse createBranch(File repoPath, Ref branchName) throws IOException, JavaGitException {
    return createBranchCommandExecute(repoPath, null, branchName, null);
  }

  public VoidResponse createBranch(File repoPath, GitBranchOptions options, Ref branchName) throws IOException, JavaGitException {
    return createBranchCommandExecute(repoPath, options, branchName, null);
  }

  public VoidResponse createBranch(File repoPath, Ref branchName, Ref startPoint) throws IOException, JavaGitException {
    return createBranchCommandExecute(repoPath, null, branchName, startPoint);
  }

  public VoidResponse createBranch(File repoPath, GitBranchOptions options, Ref branchName, Ref startPoint) throws IOException, JavaGitException {
    return createBranchCommandExecute(repoPath, options, branchName, startPoint);
  }

  private VoidResponse createBranchCommandExecute(File repositoryPath, GitBranchOptions options, Ref ref1, Ref ref2)
          throws IOException, JavaGitException {
    validator.checkNullArgument(repositoryPath, "repository path");
    processBuilder.setCommandLine(buildCommand(options, ref1, ref2, null));
    GitBranchCreateParser parser = new GitBranchCreateParser();
    parser.setWorkingDir(repositoryPath);
    commandRunner.setWorkingDirectory(repositoryPath);
    commandRunner.setParser(parser);
    commandRunner.setProcessBuilder(processBuilder);
    return (VoidResponse) commandRunner.run();
  }

  private GitBranchResponseImpl branchCommandExecute(File repoPath, GitBranchOptions options, Ref ref1, Ref ref2,
                                                     List<Ref> branchList)
          throws IOException, JavaGitException {
    CheckUtilities.checkNullArgument(repoPath, "repository path");
    List<String> commandLine = buildCommand(options, ref1, ref2, branchList);
    GitBranchParser parser = new GitBranchParser(validator);
    return new CommandRunner<GitBranchResponseImpl>(repoPath, parser, new GitProcessBuilder(commandLine)).run();
  }

  protected List<String> buildCommand(GitBranchOptions options, Ref arg1, Ref arg2,
                                      List<Ref> branchList) {
    List<String> cmd = new ArrayList<>();

      cmd.add(configuration.getGitCommand());
      cmd.add("branch");

    if (null != options) {
      Ref commit = options.getOptContains();
      if (null != commit) {
        cmd.add("--contains");
        cmd.add(commit.getName());
      }
      if (options.isOptVerbose()) {
        cmd.add("--verbose");
      }
      if (options.isOptAbbrev()) {
        if (options.getOptAbbrevLen() != GitBranchOptions.DEFAULT_ABBREV_LEN) {
          cmd.add("--abbrev=" + options.getOptAbbrevLen());
        } else {
          cmd.add("--abbrev");
        }
      }
      if (options.isOptNoAbbrev()) {
        cmd.add("--no-abbrev");
      }
      if (options.isOptA()) {
        cmd.add("-a");
      }
      if (options.isOptDLower()) {
        cmd.add("-d");
      }
      if (options.isOptMLower()) {
        cmd.add("-m");
      }
      if (options.isOptDUpper()) {
        cmd.add("-D");
      }
      if (options.isOptMUpper()) {
        cmd.add("-M");
      }
      if (options.isOptColor()) {
        cmd.add("--color");
      }
      if (options.isOptNoColor()) {
        cmd.add("--no-color");
      }
      if (options.isOptF()) {
        cmd.add("-f");
      }
      if (options.isOptL()) {
        cmd.add("-l");
      }
      if (options.isOptMerged()) {
        cmd.add("--merged");
      }
      if (options.isOptNoMerged()) {
        cmd.add("--no-merged");
      }
      if (options.isOptR()) {
        cmd.add("-r");
      }
      if (options.isOptTrack()) {
        cmd.add("--track");
      }
      if (options.isOptNoTrack()) {
        cmd.add("--no-track");
      }
    }
    if (null != branchList) {
      if ((null != arg1) || (null != arg2)) {
        throw new IllegalArgumentException();
      }
      for (Ref branch : branchList) {
        cmd.add(branch.getName());
      }
    } else {
      if (null != arg1) {
        cmd.add(arg1.getName());
      }
      if (null != arg2) {
        cmd.add(arg2.getName());
      }
    }
    return cmd;
  }

  public void setDeleteOptions(GitBranchOptions options, boolean forceDelete, boolean remote) {
    if (forceDelete) {
      options.setOptDUpper(true);
    } else {
      options.setOptDLower(true);
    }
    if (remote) {
      options.setOptR(true);
    }
  }

  public static class GitBranchParser
          implements IParser {
    private GitBranchResponseImpl response;
    private StringBuffer errorMessage = null;
    private int numLinesParsed = 0;

    public GitBranchParser(Validator validator) {
      response = new GitBranchResponseImpl(validator);
    }

    public void parseLine(String line) {
      ++numLinesParsed;
      if (null != errorMessage) {
        errorMessage.append(", line").append(numLinesParsed).append("=[").append(line).append("]");
        return;
      }
      if (line.contains("fatal:") || line.contains("error:")) {
        errorMessage = new StringBuffer();
        errorMessage.append("line1=[").append(line).append("]");
      } else {
        if (line.startsWith("Deleted branch")) {
          int indexOfBranch = line.indexOf("branch");
          String branchName = line.substring(indexOfBranch + 7, line.length()-1);
          response.setResponseType(ResponseType.MESSAGE);
          if (1 == numLinesParsed) {
            response.addMessages(line.substring(0, indexOfBranch + 6));
          }
          response.addIntoBranchList(Ref.createBranchRef(branchName));
        } else {
          handleBranchDisplay(line);
        }
      }
    }

    public void handleBranchDisplay(String line) {
      String nextWord;
      boolean isCurrentBranch = false;
      StringTokenizer st = new StringTokenizer(line);

      nextWord = st.nextToken();
      response.setResponseType(ResponseType.BRANCH_LIST);

      if ('*' == nextWord.charAt(0)) {
        isCurrentBranch = true;
        nextWord = st.nextToken();
        response.setCurrentBranch(Ref.createBranchRef(nextWord));
      }
      response.addIntoBranchList(Ref.createBranchRef(nextWord));

      if (st.hasMoreTokens()) {
        Ref branch = Ref.createBranchRef(nextWord);
        nextWord = st.nextToken();
        Ref sha1 = Ref.createSha1Ref(nextWord);
        int indexOfSha = line.indexOf(nextWord);
        String comment = line.substring(indexOfSha+nextWord.length()+1);
        BranchRecord record = new BranchRecord(branch, sha1, comment, isCurrentBranch);
        response.addIntoListOfBranchRecord(record);
      }
    }

    public void processExitCode(int code) {
    }

    public GitBranchResponse getResponse(Collection<String> output) throws JavaGitException {
      if (null != errorMessage) {
        throw new JavaGitException(404000,
                ExceptionMessageMap.getMessage("404000") + "  The git-branch error message:  { " + errorMessage.toString() + " }");
      }
      return response;
    }

    @Override
    public void setWorkingDir(File workingDir) {
      throw new UnsupportedOperationException();
    }
  }

}
