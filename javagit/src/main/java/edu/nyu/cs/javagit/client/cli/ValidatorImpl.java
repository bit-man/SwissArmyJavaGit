package edu.nyu.cs.javagit.client.cli;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.nyu.cs.javagit.utilities.CheckUtilities;
import edu.nyu.cs.javagit.utilities.ExceptionMessageMap;

public class ValidatorImpl implements Validator {
    public void checkNullArgument(File file, String message) {
        CheckUtilities.checkNullArgument(file, message);
    }

    public void checkFileValidity(File file) throws IOException {
        if (!file.exists()) {
            throw new IOException(ExceptionMessageMap.getMessage("020001") + "  { filename=["
                    + file.getName() + "] }");
        }
    }

    public boolean checkUnorderedListsEqual(List<?> l1, List<?> l2) {
        if (null == l1 && null != l2) {
            return false;
        }

        if (null != l1 && null == l2) {
            return false;
        }

        if (l1.size() != l2.size()) {
            return false;
        }

        for (Object o : l1) {
            if (!l2.contains(o)) {
                return false;
            }
        }

        for (Object o : l2) {
            if (!l1.contains(o)) {
                return false;
            }
        }

        return true;
    }

    public void checkStringArgument(String str, String variableName) {
        if (null == str) {
            throw new NullPointerException(ExceptionMessageMap.getMessage("000001") + "  { variableName=[" + variableName + "] }");
        }
        if (str.length() == 0) {
            throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000001") + "  { variableName=[" + variableName + "] }");
        }
    }
}
