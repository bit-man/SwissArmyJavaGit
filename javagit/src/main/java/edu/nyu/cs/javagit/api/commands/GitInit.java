package edu.nyu.cs.javagit.api.commands;

import edu.nyu.cs.javagit.api.Configuration;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.client.IClient;
import edu.nyu.cs.javagit.client.IGitInit;
import edu.nyu.cs.javagit.client.cli.CliClient;
import edu.nyu.cs.javagit.client.cli.Validator;
import edu.nyu.cs.javagit.utilities.CheckUtilities;

import java.io.File;
import java.io.IOException;

public class GitInit {

    private Configuration configuration;
    private Validator validator;

    public GitInit(Configuration configuration, Validator validator) {
        this.configuration = configuration;
        this.validator = validator;
    }

    public GitInitResponse init(File repositoryPath, GitInitOptions options)
            throws JavaGitException, IOException {
        CheckUtilities.checkNullArgument(repositoryPath, "repository");

        IClient client = new CliClient(configuration);
        IGitInit gitInit = client.getGitInitInstance();
        return gitInit.init(repositoryPath, options);
    }

    public GitInitResponse init(File repositoryPath)
            throws JavaGitException, IOException {
        CheckUtilities.checkNullArgument(repositoryPath, "repository");

        IClient client = new CliClient(configuration);
        IGitInit gitInit = client.getGitInitInstance();
        return gitInit.init(repositoryPath);
    }

}
