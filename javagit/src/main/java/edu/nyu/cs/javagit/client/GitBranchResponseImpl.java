/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.client;

import edu.nyu.cs.javagit.api.Ref;
import edu.nyu.cs.javagit.api.commands.GitBranchResponse;
import edu.nyu.cs.javagit.client.cli.Validator;

public class GitBranchResponseImpl extends GitBranchResponse {

  public GitBranchResponseImpl(Validator validator) {
    super(validator);
  }

  public boolean addIntoBranchList(Ref branchName) {
    return branchList.add(branchName);
  }

  public boolean addIntoListOfBranchRecord(BranchRecord record) {
    return listOfBranchRecord.add(record);
  }

  public void addMessages(String message) {
    messages.append(message);
  }

  public void setCurrentBranch(Ref currentBranch) {
    this.currentBranch = currentBranch;
  }

  public void setResponseType(ResponseType responseType) {
    this.responseType = responseType;
  }
}
