/*
 * ====================================================================
 * Copyright (c) 2008 JavaGit Project.  All rights reserved.
 *
 * This software is licensed using the GNU LGPL v2.1 license.  A copy
 * of the license is included with the distribution of this source
 * code in the LICENSE.txt file.  The text of the license can also
 * be obtained at:
 *
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * For more information on the JavaGit project, see:
 *
 *   http://www.javagit.com
 * ====================================================================
 */
package edu.nyu.cs.javagit.api.commands;

import edu.nyu.cs.javagit.api.Ref;
import edu.nyu.cs.javagit.utilities.ExceptionMessageMap;

public class GitBranchOptions {
  /*
   * Generic Options.
   */
  
  private boolean optVerbose = false;
  private boolean optTrack = false;
  private boolean optNoTrack = false;
  private boolean optColor = false;
  private boolean optNoColor = false;
  private boolean optR = false;
  private Ref optContains;
  private boolean optAbbrev = false;
  public static final int DEFAULT_ABBREV_LEN = 7;
  private int optAbbrevLen = DEFAULT_ABBREV_LEN;
  private boolean optNoAbbrev = false;

  /*
   * Non-generic options.
   */
  private boolean optA = false;
  private boolean optL = false;
  private boolean optF = false;
  private boolean optMerged = false;
  private boolean optNoMerged = false;
  private boolean optDLower = false;
  private boolean optDUpper = false;
  private boolean optMLower = false;
  private boolean optMUpper = false;
  
  public boolean isOptVerbose() {
    return optVerbose;
  }

  public void setOptVerbose(boolean optVerbose) {
    checkCanSetNoArgOption("--verbose");
    if ((!optVerbose) && (optAbbrev || optNoAbbrev)) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
              + "  --no-abbrev or --abbrev can only be used with --verbose.");
    }
    this.optVerbose = optVerbose;
  }

  public boolean isOptAbbrev() {
    return optAbbrev;
  }

  public void setOptAbbrev(boolean optAbbrev) {
    if (optVerbose) {
      if (optAbbrev && optNoAbbrev) {
        throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
            + "  --abbrev cannot be used with --no-abbrev.");
      }
      this.optAbbrev = optAbbrev;
    }
  }

  public int getOptAbbrevLen() {
    return optAbbrevLen;
  }

  public void setOptAbbrevLen(int optAbbrevLen) {
    if (isOptAbbrev()) {
      this.optAbbrevLen = optAbbrevLen;
    }
  }

  public boolean isOptNoAbbrev() {
    return optNoAbbrev;
  }

  public void setOptNoAbbrev(boolean optNoAbbrev) {
    if (optVerbose) {
      if (optAbbrev && optNoAbbrev) {
        throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
            + "  --no-abbrev cannot be used with --abbrev.");
      }
      this.optNoAbbrev = optNoAbbrev;
    }
  }
  
  public boolean isOptTrack() {
    return optTrack;
  }

  public void setOptTrack(boolean optTrack) {
    checkCanSetCreateOption("--track");
    if (optNoTrack && optTrack) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  --track cannot be used with --no-track.");
    } 
    this.optTrack = optTrack;
  }

  public boolean isOptNoTrack() {
    return optNoTrack;
  }

  public void setOptNoTrack(boolean optNoTrack) {
    checkCanSetNoArgOption("--no-track");
    if (optNoTrack && optTrack) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  --no-track cannot be used with --track.");
    } 
    this.optNoTrack = optNoTrack;
  }

  public boolean isOptColor() {
    return optColor;
  }

  public void setOptColor(boolean optColor) {
    checkCanSetNoArgOption("--color");
    if (optNoColor && optColor) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  --color cannot be used with --no-color.");
    } 
    this.optColor = optColor;
  }

  public boolean isOptNoColor() {
    return optNoColor;
  }

  public void setOptNoColor(boolean optNoColor) {
    checkCanSetNoArgOption("--no-color");
    if (optNoColor && optColor) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  --no-color cannot be used with --color.");
    } 
    this.optNoColor = optNoColor;
  }

  public boolean isOptR() {
    return optR;
  }

  public void setOptR(boolean optR) {
    if (optR) {
      checkCanSetNoArgOption("-r");
    }
    this.optR = optR;
  }

  public Ref getOptContains() {
    return optContains;
  }

  public void setOptContains(Ref commit) {
    if (null != commit) {
      checkCanSetNoArgOption("--contains");
    }
    this.optContains = commit;
  }

  public boolean isOptA() {
    return optA;
  }

  public void setOptA(boolean optA) {
    checkCanSetNoArgOption("-a");
    this.optA = optA;
  }

  public boolean isOptL() {
    return optL;
  }

  public void setOptL(boolean optL) {
    checkCanSetCreateOption("-l");
    this.optL = optL;
  }

  public boolean isOptF() {
    return optF;
  }

  public void setOptF(boolean optF) {
    checkCanSetCreateOption("-f");
    this.optF = optF;
  }

  public boolean isOptMerged() {
    return optMerged;
  }

  public void setOptMerged(boolean optMerged) {
    checkCanSetNoArgOption("--merged");
    if (optNoMerged && optMerged) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  --merged cannot be used with --no-merged.");
    } 
    this.optMerged = optMerged;
  }

  public boolean isOptNoMerged() {
    return optNoMerged;
  }

  public void setOptNoMerged(boolean optNoMerged) {
    checkCanSetNoArgOption("--no-merged");
    if (optNoMerged && optMerged) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  --no-merged cannot be used with --merged.");
    } 
    this.optNoMerged = optNoMerged;
  }
  
  public boolean isOptDLower() {
    return optDLower;
  }

  public void setOptDLower(boolean optDLower) {
    checkCanSetDeleteOption("-d");
    if (optDLower && optDUpper) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  -d cannot be used with -D.");
    } 
    this.optDLower = optDLower;
  }

  public boolean isOptDUpper() {
    return optDUpper;
  }

  public void setOptDUpper(boolean optDUpper) {
    checkCanSetDeleteOption("-D");
    if (optDLower && optDUpper) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  -D cannot be used with -d.");
    } 
    this.optDUpper = optDUpper;
  }

  public boolean isOptMLower() {
    return optMLower;
  }

  public void setOptMLower(boolean optMLower) {
    checkCanSetRenameOption("-m");
    if (optMLower && optMUpper) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  -m cannot be used with -M.");
    } 
    this.optMLower = optMLower;
  }

  public boolean isOptMUpper() {
    return optMUpper;
  }

  public void setOptMUpper(boolean optMUpper) {
    checkCanSetRenameOption("-M");
    if (optMLower && optMUpper) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + "  -M cannot be used with -m.");
    } 
    this.optMUpper = optMUpper;
  }

  public void checkCanSetNoArgOption(String option) {
    if (isOptTrack() || isOptNoTrack() || isOptL() || isOptF() || isOptDLower() || isOptDUpper()
        || isOptMLower() || isOptMUpper()) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120")
          + option + " should be used without arguments, to display branches");
    } 
  }
  
  public void checkCanSetCreateOption(String option) {
    if (isOptColor() || isOptNoColor() || isOptR() || isOptA() || isOptVerbose() ||
        isOptMerged() || isOptNoMerged() || (null != getOptContains()) || isOptMLower() || 
        isOptMUpper() || isOptDLower() || isOptDUpper()) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120") + option + 
      " should be used with a branch name and optional start point, to create a branch");
    }
  }
  
  public void checkCanSetDeleteOption(String option) {
    if (isOptColor() || isOptNoColor() || isOptA() || isOptVerbose() || isOptMerged() || 
        isOptNoMerged() || (null != getOptContains()) || isOptTrack() || isOptNoTrack() || 
        isOptL() || isOptF() || isOptMLower() || isOptMUpper()) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120") + option + 
      " should be used with branch(es), to delete the branch(es).");
    }
  }
  
  public void checkCanSetRenameOption(String option) {
    if (isOptColor() || isOptNoColor() || isOptR() || isOptA() || isOptVerbose() ||
        isOptMerged() || isOptNoMerged() || (null != getOptContains()) || isOptTrack() || 
        isOptNoTrack() || isOptL() || isOptF() || isOptDLower() || isOptDUpper()) {
      throw new IllegalArgumentException(ExceptionMessageMap.getMessage("000120") + option + 
      " should be used with optional oldbranch and newbranch, to rename oldbranch/current branch" +
      "to newbranch.");
    }
  }
} 
